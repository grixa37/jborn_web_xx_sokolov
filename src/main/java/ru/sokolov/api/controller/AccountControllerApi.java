package ru.sokolov.api.controller;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.sokolov.api.converter.AccountDtoToResponseConverter;
import ru.sokolov.api.json.*;
import ru.sokolov.entity.UserModel;
import ru.sokolov.repository.UserModelRepository;
import ru.sokolov.security.CustomUserDetails;
import ru.sokolov.dto.AccountDTO;
import ru.sokolov.service.AccountService;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RequiredArgsConstructor
@RestController
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequestMapping("/api")

public class AccountControllerApi {
    AccountService accountService;
    AccountDtoToResponseConverter converter;
    UserModelRepository userModelRepository;

    public static final String ACCOUNTS = "/accounts";
    public static final String CREATE_ACCOUNT = "/accounts/account";
    public static final String DELETE_ACCOUNT = "/accounts/account/{account_id}";

    @GetMapping(ACCOUNTS)
    public ResponseEntity<AccountResponse> getAccounts() {
        UserModel user = currentUser();
        assert user != null;
        List<AccountDTO> accounts = accountService.getAccounts(user.getId());

        return ok(new AccountResponse(accounts));
    }

    @PostMapping(CREATE_ACCOUNT)
    public ResponseEntity<AddAccountResponse> addAccount(@RequestBody @Valid AccountRequest accountRequest) {
        UserModel user = currentUser();
        assert user != null;
        AccountDTO accountDTO = accountService.addAccount(
                accountRequest.getNameAccount(),
                user.getId(),
                accountRequest.getBalance());

        return ok(converter.convert(accountDTO));
    }

    @DeleteMapping(DELETE_ACCOUNT)
    public ResponseEntity<DeleteAccountResponse> delAccount(@PathVariable long account_id) {
        if (accountService.deleteAccount(account_id)) {

            return ok(new DeleteAccountResponse("Success!"));
        } else {

            return ok(new DeleteAccountResponse("Not success!"));
        }
    }

    private UserModel currentUser() {
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        if (principal instanceof CustomUserDetails) {
            CustomUserDetails user = (CustomUserDetails) principal;

            return userModelRepository.getOne(user.getId());
        }

        return null;
    }
}