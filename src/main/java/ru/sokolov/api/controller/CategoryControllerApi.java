package ru.sokolov.api.controller;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import ru.sokolov.api.converter.CategoryDtoToResponseConverter;
import ru.sokolov.api.json.*;
import ru.sokolov.entity.UserModel;
import ru.sokolov.repository.UserModelRepository;
import ru.sokolov.security.CustomUserDetails;
import ru.sokolov.dto.CategoryDTO;
import ru.sokolov.service.CategoryService;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.ResponseEntity.ok;

@RequiredArgsConstructor
@RestController
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequestMapping("/api")

public class CategoryControllerApi {
    CategoryService categoryService;
    CategoryDtoToResponseConverter converter;
    UserModelRepository userModelRepository;

    public static final String CREATE_CATEGORY = "/categories";
    public static final String UPDATE_CATEGORY = "/categories";
    public static final String DELETE_CATEGORY = "/categories/category/{category_id}";
    public static final String CREATE_REPORTS = "/reports";

    @PostMapping(CREATE_CATEGORY)
    public ResponseEntity<CategoryResponse> addCategory(@RequestBody @Valid CategoryRequest categoryRequest) {
        UserModel user = currentUser();
        assert user != null;
        CategoryDTO categoryDTO = categoryService.addCategory(categoryRequest.getCategoryName(), user.getId());

        return ok(converter.convert(categoryDTO));
    }

    @PutMapping(UPDATE_CATEGORY)
    public ResponseEntity<CategoryResponse> editCategory(@RequestBody @Valid EditCategoryRequest editCategoryRequest) {
        CategoryDTO categoryDTO = categoryService.editCategory(
                editCategoryRequest.getId(),
                editCategoryRequest.getNameCategory());

        return ok(converter.convert(categoryDTO));
    }

    @DeleteMapping(DELETE_CATEGORY)
    public ResponseEntity<DeleteCategoryResponse> delCategory(@PathVariable(value = "category_id") long categoryId) {
        if (categoryService.deleteCategory(categoryId)) {

            return ok(new DeleteCategoryResponse("Success!"));
        } else {

            return ok(new DeleteCategoryResponse("Not success!"));
        }
    }

    @PostMapping(CREATE_REPORTS)
    public ResponseEntity<ReportsCategoryResponse> getReports(
            @RequestBody @Valid ReportsCategoryRequest reportsCategoryRequest) {
        List<String> reports = categoryService.reports(
                reportsCategoryRequest.getId(),
                reportsCategoryRequest.getTimestampFrom(),
                reportsCategoryRequest.getTimestampTo());

        return ok(new ReportsCategoryResponse(reports));
    }

    private UserModel currentUser() {
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        if (principal instanceof CustomUserDetails) {
            CustomUserDetails user = (CustomUserDetails) principal;

            return userModelRepository.getOne(user.getId());
        }

        return null;
    }
}
