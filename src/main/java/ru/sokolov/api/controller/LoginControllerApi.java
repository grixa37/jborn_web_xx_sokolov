package ru.sokolov.api.controller;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sokolov.api.converter.UserDtoToResponseConverter;
import ru.sokolov.api.json.AuthRequest;
import ru.sokolov.api.json.AuthResponse;
import ru.sokolov.service.AuthService;
import ru.sokolov.service.UserDTO;

import javax.validation.Valid;

import static org.springframework.http.ResponseEntity.ok;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RestController
@RequestMapping("/api")

public class LoginControllerApi {
    AuthService authService;
    UserDtoToResponseConverter converter;

    public static final String REGISTRATION  = "/registration";

    @PostMapping(REGISTRATION)
    public ResponseEntity<AuthResponse> registration(@RequestBody @Valid AuthRequest authRequest) {
        UserDTO user = authService.registration(
                authRequest.getName(),
                authRequest.getLastName(),
                authRequest.getEmail(),
                authRequest.getPassword());

        return ok(converter.convert(user));
    }
}

