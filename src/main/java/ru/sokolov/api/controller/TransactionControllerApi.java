package ru.sokolov.api.controller;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.sokolov.api.converter.TransactionDtoToResponseConverter;
import ru.sokolov.api.json.TransactionRequest;
import ru.sokolov.api.json.TransactionResponse;
import ru.sokolov.service.TransactionDTO;
import ru.sokolov.service.TransactionService;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.util.Date;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@RequiredArgsConstructor
@RequestMapping("/api")
public class TransactionControllerApi {
    TransactionService transactionService;
    TransactionDtoToResponseConverter converter;

    public static final String CREATE_TRANSACTION  = "/transactions";

    @PostMapping(CREATE_TRANSACTION)
    public ResponseEntity<TransactionResponse> addTransaction(
            @RequestBody @Valid TransactionRequest transactionRequest) {
        TransactionDTO transactionDTO = transactionService.addTransaction(
                transactionRequest.getComment(),
                transactionRequest.getFromAccountId(),
                transactionRequest.getToAccountId(),
                transactionRequest.getAmount(),
                new Timestamp(new Date().getTime()),
                transactionRequest.getCategoriesId()
        );

        return ok(converter.convert(transactionDTO));
    }
}
