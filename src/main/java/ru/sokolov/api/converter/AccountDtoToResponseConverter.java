package ru.sokolov.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.sokolov.api.json.AddAccountResponse;
import ru.sokolov.dto.AccountDTO;

@Component
public class AccountDtoToResponseConverter implements Converter<AccountDTO, AddAccountResponse> {
    @Override
    public AddAccountResponse convert(AccountDTO accountDTO) {

        return new AddAccountResponse(
                accountDTO.getId(),
                accountDTO.getNameAccount(),
                accountDTO.getId(),
                accountDTO.getBalance()
        );
    }
}
