package ru.sokolov.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.sokolov.entity.AccountModel;
import ru.sokolov.dto.AccountDTO;

@Component
public class AccountModelToAccountDTOConvert implements Converter<AccountModel, AccountDTO> {
    @Override
    public AccountDTO convert(AccountModel accountModel) {

        return new AccountDTO()
                .setId(accountModel.getId())
                .setNameAccount(accountModel.getNameAccount())
                .setUserId(accountModel.getUser().getId())
                .setBalance(accountModel.getBalance()
                );
    }
}
