package ru.sokolov.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.sokolov.api.json.CategoryResponse;
import ru.sokolov.dto.CategoryDTO;

@Component
public class CategoryDtoToResponseConverter implements Converter<CategoryDTO, CategoryResponse> {
    @Override
    public CategoryResponse convert(CategoryDTO categoryDTO) {

        return new CategoryResponse(
                categoryDTO.getId(),
                categoryDTO.getId(),
                categoryDTO.getNameCategory()
        );
    }
}
