package ru.sokolov.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.sokolov.entity.CategoryModel;
import ru.sokolov.dto.CategoryDTO;

@Component
public class CategoryModelToCategoryDTOConverter implements Converter<CategoryModel, CategoryDTO> {
    @Override
    public CategoryDTO convert(CategoryModel categoryModel) {

        return CategoryDTO.builder()
                .id(categoryModel.getId())
                .userId(categoryModel.getUser().getId())
                .nameCategory(categoryModel.getNameCategory())
                .build();
    }
}
