package ru.sokolov.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.sokolov.api.json.TransactionResponse;
import ru.sokolov.service.TransactionDTO;

@Component
public class TransactionDtoToResponseConverter implements Converter<TransactionDTO, TransactionResponse> {
    @Override
    public TransactionResponse convert(TransactionDTO transactionDTO) {

        return new TransactionResponse(
                transactionDTO.getId(),
                transactionDTO.getComment(),
                transactionDTO.getFromAccountId(),
                transactionDTO.getToAccountId(),
                transactionDTO.getAmount(),
                transactionDTO.getTimestamp(),
                transactionDTO.getCategoriesId()
        );
    }
}
