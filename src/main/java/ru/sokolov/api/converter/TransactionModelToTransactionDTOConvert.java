package ru.sokolov.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.sokolov.entity.CategoryModel;
import ru.sokolov.entity.TransactionModel;
import ru.sokolov.service.TransactionDTO;

import java.util.stream.Collectors;

@Component
public class TransactionModelToTransactionDTOConvert implements Converter<TransactionModel, TransactionDTO> {
    @Override
    public TransactionDTO convert(TransactionModel transactionModel) {

        return TransactionDTO.builder()
                .id(transactionModel.getId())
                .comment(transactionModel.getComment())
                .fromAccountId(transactionModel.getFromAccount() == null ? null : transactionModel.getFromAccount().getId())
                .toAccountId(transactionModel.getToAccount() == null ? null :  transactionModel.getToAccount().getId())
                .amount(transactionModel.getAmount())
                .timestamp(transactionModel.getDate())
                .categoriesId(transactionModel.getCategories().stream().map(CategoryModel::getId).collect(Collectors.toList()))
                .build();
    }
}
