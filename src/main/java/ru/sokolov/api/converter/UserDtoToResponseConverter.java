package ru.sokolov.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.sokolov.api.json.AuthResponse;
import ru.sokolov.service.UserDTO;

@Component
public class UserDtoToResponseConverter implements Converter<UserDTO, AuthResponse> {
    @Override
    public AuthResponse convert(UserDTO userDTO) {

        return new AuthResponse(
                userDTO.getId(),
                userDTO.getName(),
                userDTO.getLastName(),
                userDTO.getEmail()
        );
    }
}
