package ru.sokolov.api.converter;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ru.sokolov.entity.UserModel;
import ru.sokolov.service.UserDTO;

@Component
public class UserModelToUserDTOConverter implements Converter<UserModel, UserDTO> {
    @Override
    public UserDTO convert(UserModel userModel) {

        return new UserDTO(
                userModel.getId(),
                userModel.getEmail(),
                userModel.getName(),
                userModel.getLastName()
        );
    }
}
