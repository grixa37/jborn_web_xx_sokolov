package ru.sokolov.api.json;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AccountRequest {
    @NotBlank
    String nameAccount;

    @NotNull
    BigDecimal balance;
}
