package ru.sokolov.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.sokolov.dto.AccountDTO;

import java.util.List;

@Data
@AllArgsConstructor
public class AccountResponse {
    private List<AccountDTO> accounts;
}
