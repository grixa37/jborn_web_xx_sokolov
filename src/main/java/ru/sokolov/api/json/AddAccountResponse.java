package ru.sokolov.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class AddAccountResponse {
    private Long id;
    private String nameAccount;
    private Long userId;
    private BigDecimal balance;
}
