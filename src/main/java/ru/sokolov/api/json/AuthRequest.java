package ru.sokolov.api.json;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
public class AuthRequest {

    private String name;

    private String lastName;

    @Email
    @NotBlank
    private String email;

    @NotBlank
    private String password;
}
