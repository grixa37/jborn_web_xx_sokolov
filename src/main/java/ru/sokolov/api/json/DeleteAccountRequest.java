package ru.sokolov.api.json;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class DeleteAccountRequest {
    @NotNull
    private Long id;
}
