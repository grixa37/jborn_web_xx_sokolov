package ru.sokolov.api.json;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class DeleteCategoryRequest {
    @NotNull
    private Long id;
}
