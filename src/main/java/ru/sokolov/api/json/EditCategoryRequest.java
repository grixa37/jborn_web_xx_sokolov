package ru.sokolov.api.json;

import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
public class EditCategoryRequest {
    @NotNull
    private Long id;

    @NotBlank
    private String nameCategory;
}
