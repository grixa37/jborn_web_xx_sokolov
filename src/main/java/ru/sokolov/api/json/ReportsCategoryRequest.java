package ru.sokolov.api.json;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.sql.Timestamp;

@Data
public class ReportsCategoryRequest {
    @NotNull
    private Long id;

    @NotNull
    private Timestamp timestampFrom;

    @NotNull
    private Timestamp timestampTo;
}
