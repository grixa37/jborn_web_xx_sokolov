package ru.sokolov.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ReportsCategoryResponse {
    private List<String> reports;
}
