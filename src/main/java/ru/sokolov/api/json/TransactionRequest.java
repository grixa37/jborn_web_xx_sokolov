package ru.sokolov.api.json;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Getter
@Setter
public class TransactionRequest {

    private String comment;

    private Long fromAccountId;

    private Long toAccountId;

    @NotNull
    private BigDecimal amount;

    private Timestamp timestamp;

    private List<Long> categoriesId;
}
