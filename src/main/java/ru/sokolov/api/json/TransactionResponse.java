package ru.sokolov.api.json;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Data
@AllArgsConstructor
public class TransactionResponse {
    private Long id;

    private String comment;

    private Long fromAccountId;

    private Long toAccountId;

    private BigDecimal amount;

    private Timestamp timestamp;

    private List<Long> categoriesId;
}
