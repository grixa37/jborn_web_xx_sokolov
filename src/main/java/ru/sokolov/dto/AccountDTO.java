package ru.sokolov.dto;

import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AccountDTO {

    @NonNull
    Long id;

    @NonNull
    String nameAccount;

    @NonNull
    Long userId;

    @NonNull
    BigDecimal balance;
}
