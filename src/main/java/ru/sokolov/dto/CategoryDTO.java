package ru.sokolov.dto;

import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CategoryDTO {

    @NonNull
    Long id;

    @NonNull
    Long userId;

    @NonNull
    String nameCategory;
}
