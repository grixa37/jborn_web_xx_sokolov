package ru.sokolov.entity;

import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "account")
public class AccountModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "name_account")
    String nameAccount;

    @ManyToOne
    @JoinColumn(name = "user_id")
    UserModel user;

    @Column(name = "balance")
    BigDecimal balance;

    public static AccountModel makeDefault(String nameAccount, UserModel user, BigDecimal balance) {
        return builder()
                .nameAccount(nameAccount)
                .user(user)
                .balance(balance)
                .build();
    }

    public static AccountModel makeEmpty(){
        return builder()
                .id(0L)
                .nameAccount("пусто")
                .build();
    }

    @Override
    public String toString() {
        if (id == null || nameAccount == null) {
            return "";
        }
        return id + " " + nameAccount;
    }
}
