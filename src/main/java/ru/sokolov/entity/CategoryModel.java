package ru.sokolov.entity;

import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "category")
public class CategoryModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @ManyToOne
    @JoinColumn(name = "user_id")
    UserModel user;

    @Column(name = "name_category", nullable = false)
    String nameCategory;

    public static CategoryModel createDefault(String nameCategory, UserModel user) {
        return builder()
                .nameCategory(nameCategory)
                .user(user)
                .build();
    }
}
