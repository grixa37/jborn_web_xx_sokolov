package ru.sokolov.entity;

import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name = "Transaction")
public class TransactionModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "comment")
    String comment;

    @ManyToOne
    @JoinColumn(name = "from_account_id")
    AccountModel fromAccount;

    @ManyToOne
    @JoinColumn(name = "to_account_id")
    AccountModel toAccount;

    @Column(name = "amount", nullable = false)
    BigDecimal amount;

    @Column(name = "date", nullable = false)
    Timestamp date;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "category_to_transaction",
            joinColumns = @JoinColumn(name = "transaction_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "category_id", referencedColumnName = "id")
    )
    List<CategoryModel> categories;
}
