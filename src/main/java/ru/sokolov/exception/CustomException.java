package ru.sokolov.exception;

public class CustomException extends RuntimeException {

    public CustomException(Throwable e) {
        super(e);
    }

    public CustomException(String message) {
        super(message);
    }
}
