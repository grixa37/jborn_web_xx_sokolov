package ru.sokolov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sokolov.entity.AccountModel;
import ru.sokolov.entity.UserModel;

import java.util.List;
import java.util.Optional;

public interface AccountModelRepository extends JpaRepository<AccountModel, Long> {
    Optional<List<AccountModel>> findAllByUserOrderById(UserModel user);
}
