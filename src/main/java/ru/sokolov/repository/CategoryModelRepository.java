package ru.sokolov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sokolov.entity.CategoryModel;
import ru.sokolov.entity.UserModel;

import java.util.Collection;
import java.util.List;

public interface CategoryModelRepository extends JpaRepository<CategoryModel, Long> {
    List<CategoryModel> findAllByUserOrderById(UserModel user);
    List<CategoryModel> findAllByIdIn(Collection<Long> id);
}
