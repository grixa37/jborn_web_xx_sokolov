package ru.sokolov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sokolov.entity.CategoryModel;
import ru.sokolov.entity.TransactionModel;

import java.sql.Timestamp;
import java.util.List;

public interface TransactionModelRepository extends JpaRepository<TransactionModel, Long> {
    List<TransactionModel> findDistinctByCategoriesInAndDateAfterAndDateBeforeOrderById(List<CategoryModel> categories, Timestamp dateAfter, Timestamp dateBefore);
}
