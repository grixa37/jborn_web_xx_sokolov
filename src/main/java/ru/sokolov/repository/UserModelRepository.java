package ru.sokolov.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.sokolov.entity.UserModel;

public interface UserModelRepository extends JpaRepository<UserModel, Long> {
    UserModel findByEmail(String email);
}
