CREATE TABLE users
(
    id        int primary key,
    name      varchar(50) not null,
    last_name varchar(50) not null,
    email     varchar(50) not null,
    password  varchar(50) not null
);
CREATE TABLE account
(
    id           int primary key,
    name_account varchar(50)    not null,
    user_id      int references users (id),
    balance      decimal(10, 2) not null
);
CREATE TABLE transaction
(
    id              int primary key,
    comment         varchar(50),
    from_account_id int references account (id),
    to_account_id   int references account (id),
    amount          decimal(10, 2) not null,
    date            timestamp      not null
);
CREATE TABLE category
(
    id            int primary key,
    user_id       int references users (id),
    name_category varchar(50) not null
);
CREATE TABLE category_to_transaction
(
    category_id    int references category (id),
    transaction_id int references transaction (id)
);

select *
from account
where user_id = 1;

select account.user_id, transaction.comment, transaction.amount, transaction.date
from transaction
         join account on account.id = transaction.from_account_id
    or account.id = transaction.to_account_id
where account.user_id = 1
  and date between (current_date - interval '1' day)
  and current_date;

select users.name, users.last_name, sum(account.balance)
from account
         join users
              on users.id = account.user_id
group by user_id;
