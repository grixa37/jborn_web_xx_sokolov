package ru.sokolov.security;

import lombok.EqualsAndHashCode;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

@EqualsAndHashCode
@RequiredArgsConstructor
public class CustomGrantedAuthority implements GrantedAuthority {
    public static final String PREFIX = "ROLE_";

    private final UserRole userRole;

    @Override
    public String getAuthority() {
        return PREFIX + userRole.name();
    }
}
