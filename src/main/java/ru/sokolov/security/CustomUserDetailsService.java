package ru.sokolov.security;

import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.sokolov.entity.UserModel;
import ru.sokolov.repository.UserModelRepository;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {
    private final UserModelRepository userModelRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        UserModel userModel = userModelRepository.findByEmail(email);
        if (userModel == null) {
            throw new UsernameNotFoundException("Can't find user with email " + email);
        }
        return new CustomUserDetails(
                userModel.getId(),
                userModel.getEmail(),
                userModel.getPassword(),
                userModel.getRoles()
                        .stream()
                        .map(CustomGrantedAuthority::new)
                        .collect(Collectors.toList())
        );
    }
}
