package ru.sokolov.security;

public enum UserRole {
    USER,
    ADMIN
}
