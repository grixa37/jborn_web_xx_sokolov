package ru.sokolov.service;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import ru.sokolov.dto.AccountDTO;
import ru.sokolov.entity.AccountModel;
import ru.sokolov.entity.UserModel;
import ru.sokolov.repository.AccountModelRepository;
import ru.sokolov.repository.UserModelRepository;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AccountService {
    private final AccountModelRepository accountModelRepository;
    private final UserModelRepository userModelRepository;
    private final Converter<AccountModel, AccountDTO> accountModelToAccountDTOConvert;

    public List<AccountDTO> getAccounts(Long userId) {
        UserModel user = userModelRepository.getOne(userId);
        List<AccountModel> accountsModel = accountModelRepository
                .findAllByUserOrderById(user)
                .orElse(Collections.emptyList());

        return accountsModel.stream().map(accountModelToAccountDTOConvert::convert).collect(Collectors.toList());
    }

    public AccountDTO addAccount(String nameAccount, Long userId, BigDecimal balance) {
        UserModel user = userModelRepository.getOne(userId);
        AccountModel accountModel = AccountModel.builder()
                .nameAccount(nameAccount)
                .user(user)
                .balance(balance)
                .build();
        accountModel = accountModelRepository.save(accountModel);

        return accountModelToAccountDTOConvert.convert(accountModel);
    }

    public boolean deleteAccount(Long id) {
        AccountModel accountModel = accountModelRepository.getOne(id);
        if (accountModel.getId() != null) {
            accountModelRepository.delete(accountModel);

            return true;
        } else {

            return false;
        }
    }

}
