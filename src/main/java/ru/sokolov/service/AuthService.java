package ru.sokolov.service;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.sokolov.entity.UserModel;
import ru.sokolov.repository.UserModelRepository;
import ru.sokolov.security.UserRole;

import java.util.Collections;
import java.util.HashSet;

@Service
@RequiredArgsConstructor
public class AuthService {
    private final UserModelRepository userModelRepository;
    private final PasswordEncoder passwordEncoder;
    private final Converter<UserModel, UserDTO> userModelToUserDTOConverter;

    public UserDTO auth(String email) {
        UserModel userModel = userModelRepository.findByEmail(email);
        return userModel == null ? null : userModelToUserDTOConverter.convert(userModel);
    }

    public UserDTO registration(String name, String lastName, String email, String password) {
        String passwordHash = passwordEncoder.encode(password);
        UserModel userModel = new UserModel()
                .setName(name)
                .setLastName(lastName)
                .setEmail(email)
                .setPassword(passwordHash)
                .setRoles(new HashSet<>(Collections.singleton(UserRole.USER)));
        userModel = userModelRepository.save(userModel);

        return userModelToUserDTOConverter.convert(userModel);
    }
}
