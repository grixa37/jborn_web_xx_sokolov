package ru.sokolov.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.sokolov.api.converter.CategoryModelToCategoryDTOConverter;
import ru.sokolov.dto.CategoryDTO;
import ru.sokolov.entity.CategoryModel;
import ru.sokolov.entity.TransactionModel;
import ru.sokolov.entity.UserModel;
import ru.sokolov.repository.CategoryModelRepository;
import ru.sokolov.repository.TransactionModelRepository;
import ru.sokolov.repository.UserModelRepository;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CategoryService {
    private final CategoryModelRepository categoryModelRepository;
    private final UserModelRepository userModelRepository;
    private final TransactionModelRepository transactionModelRepository;
    private final CategoryModelToCategoryDTOConverter converter;

    public List<CategoryDTO> getCategories(Long userId) {
        UserModel user = userModelRepository.getOne(userId);
        List<CategoryModel> categories = categoryModelRepository.findAllByUserOrderById(user);

        return categories.stream().map(converter::convert).collect(Collectors.toList());
    }

    public CategoryDTO addCategory(String nameCategory, Long userId) {
        UserModel user = userModelRepository.getOne(userId);
        CategoryModel categoryModel = categoryModelRepository.saveAndFlush(
                CategoryModel.createDefault(nameCategory, user)
        );

        return  converter.convert(categoryModel);
    }

    public CategoryDTO editCategory(Long id, String nameCategory) {
        CategoryModel categoryModel = categoryModelRepository.getOne(id).setNameCategory(nameCategory);
        categoryModel = categoryModelRepository.save(categoryModel);

        return converter.convert(categoryModel);
    }

    public Boolean deleteCategory(Long id) {
        CategoryModel categoryModel = categoryModelRepository.getOne(id);
        if (categoryModel.getId() != null) {
            categoryModelRepository.delete(categoryModel);

            return true;
        } else {

            return false;
        }
    }

    public List<String> reports(Long id, Timestamp after, Timestamp before) {
        CategoryModel categoryModel = categoryModelRepository.getOne(id);
        List<CategoryModel> categories = new ArrayList<>();
        categories.add(categoryModel);
        List<TransactionModel> transactionModelList = transactionModelRepository
                .findDistinctByCategoriesInAndDateAfterAndDateBeforeOrderById(categories, after, before);

        return transactionModelList.stream().map(transactionModel ->
                        categoryModel.getNameCategory() + " "
                                + transactionModel.getComment() + " "
                                + transactionModel.getAmount())
                .collect(Collectors.toList());
    }
}
