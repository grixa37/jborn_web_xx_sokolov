package ru.sokolov.service;

public interface DigestService {
    String hex(String str);
}
