package ru.sokolov.service;

import lombok.*;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Data
@Builder
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TransactionDTO {
    Long id;

    String comment;

    Long fromAccountId;

    Long toAccountId;

    BigDecimal amount;

    Timestamp timestamp;

    List<Long> categoriesId;
}
