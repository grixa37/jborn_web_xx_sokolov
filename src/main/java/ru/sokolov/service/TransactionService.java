package ru.sokolov.service;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.sokolov.entity.AccountModel;
import ru.sokolov.entity.CategoryModel;
import ru.sokolov.entity.TransactionModel;
import ru.sokolov.repository.AccountModelRepository;
import ru.sokolov.repository.CategoryModelRepository;
import ru.sokolov.repository.TransactionModelRepository;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class TransactionService {
    private final TransactionModelRepository transactionModelRepository;
    private final AccountModelRepository accountModelRepository;
    private final CategoryModelRepository categoryModelRepository;
    private final Converter<TransactionModel, TransactionDTO> transactionModelToTransactionDTOConvert;

    public TransactionDTO addTransaction(String comment, Long fromAccountId, Long toAccountId, BigDecimal amount, Timestamp timestamp, List<Long> categoriesId) {
        if (fromAccountId != null || toAccountId != null) {
            List<CategoryModel> categories = categoryModelRepository.findAllByIdIn(categoriesId);
            TransactionModel transactionModel = TransactionModel.builder()
                    .comment(comment)
                    .amount(amount)
                    .date(timestamp)
                    .categories(categories)
                    .build();
            if (fromAccountId != null) {
                AccountModel fromAccount = accountModelRepository.getOne(fromAccountId);
                fromAccount.setBalance(fromAccount.getBalance().subtract(amount));
                transactionModel.setFromAccount(fromAccount);
                accountModelRepository.save(fromAccount);
            }
            if (toAccountId != null) {
                AccountModel toAccount = accountModelRepository.getOne(toAccountId);
                toAccount.setBalance(toAccount.getBalance().add(amount));
                transactionModel.setToAccount(toAccount);
                accountModelRepository.save(toAccount);
            }
            transactionModelRepository.saveAndFlush(transactionModel);

            return transactionModelToTransactionDTOConvert.convert(transactionModel);
        }

        return TransactionDTO.builder().build();
    }
}
