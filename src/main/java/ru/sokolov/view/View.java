package ru.sokolov.view;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import ru.sokolov.dto.AccountDTO;
import ru.sokolov.dto.CategoryDTO;
import ru.sokolov.service.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@RequiredArgsConstructor
@Component
public class View implements CommandLineRunner {
    private final AuthService authService;
    private final AccountService accountService;
    private final CategoryService categoryService;
    private final TransactionService transactionService;

    public void start() {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                menu();
                String string = getString("1, 2 или 0", br);
                if ("1".equals(string)) {
                    UserDTO userDTO = authService.registration(
                            getString("Имя", br),
                            getString("Фамилия", br),
                            getString("Email", br),
                            getString("Пароль", br));
                    successOrError(userDTO != null);
                } else if ("2".equals(string)) {
                    UserDTO userDTO = authService.auth(getString("Email", br));
                    if (userDTO != null) {
                        System.out.println("Успех!");
                        personalAccount(userDTO, br);
                    } else {
                        System.out.println("Нет такого пользователя.");
                    }
                } else if ("0".equals(string)) {
                    System.out.println("Пока!");
                    break;
                }
            }
        } catch (IOException | SQLException | ParseException e) {
            e.printStackTrace();
        }
    }

    private void menu() {
        System.out.println("1. Регистрация");
        System.out.println("2. Авторизация");
        System.out.println("0. Выход");
    }

    private void menuPA() {
        System.out.println("Личный кабинет:");
        System.out.println("1. Вывести список счетов");
        System.out.println("2. Создать счёт");
        System.out.println("3. Удалить счёт");
        System.out.println("4. Категории");
        System.out.println("5. Отчеты");
        System.out.println("6. Транзакции");
        System.out.println("0. Выход");
    }

    private void menuCategory() {
        System.out.println("Категории:");
        System.out.println("1. Добавить");
        System.out.println("2. Редактировать");
        System.out.println("3. Удалить");
        System.out.println("0. Выход");
    }

    private void menuReports() {
        System.out.println("Отчеты:");
        System.out.println("1. По категориям");
        System.out.println("0. Выход");
    }

    private void menuTransaction() {
        System.out.println("Транзакции:");
        System.out.println("1. Добавить");
        System.out.println("0. Выход");
    }

    private String getString(String str, BufferedReader br) throws IOException {
        System.out.println("Введите " + str + ":");
        return br.readLine();
    }

    private Long getLong(String str) {
        return str.isEmpty() ? null : Long.parseLong(str);
    }

    private void personalAccount(UserDTO userDTO, BufferedReader br) throws SQLException, IOException, ParseException {
        while (true) {
            menuPA();
            String string = getString("1, 2, 3, 4 или 0", br);
            if ("1".equals(string)) {
                List<AccountDTO> accounts = accountService.getAccounts(userDTO.getId());
                accounts.forEach(System.out::println);
                System.out.println();
            } else if ("2".equals(string)) {
                AccountDTO accountDTO = accountService.addAccount(
                        getString("Название счета", br),
                        userDTO.getId(),
                        new BigDecimal(getString("Баланс", br)));
                successOrError(accountDTO != null);
            } else if ("3".equals(string)) {
                successOrError(accountService.deleteAccount(getLong("ID счета")));
            } else if ("4".equals(string)) {
                categories(userDTO, br);
            } else if ("5".equals(string)) {
                reports(br);
            } else if ("6".equals(string)) {
                transactions(br);
            } else if ("0".equals(string)) {
                break;
            }
        }
    }

    private void reports(BufferedReader br) throws IOException, ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-M-yyyy");
        while (true) {
            menuReports();
            String string = getString("1 или 0", br);
            if ("1".equals(string)) {
                List<String> reports = categoryService.reports(
                        getLong(getString("Id категории", br)),
                        new Timestamp(formatter.parse(getString("дату(дд-мм-гггг)", br)).getTime()),
                        new Timestamp(formatter.parse(getString("дату(дд-мм-гггг)", br)).getTime()));
                if (!reports.isEmpty()) {
                    reports.forEach(System.out::println);
                } else {
                    System.out.println("Ничего не найдено");
                }
            } else if ("0".equals(string)) {
                break;
            }
        }
    }

    private void categories(UserDTO userDTO, BufferedReader br) throws IOException {
        while (true) {
            menuCategory();
            String string = getString("1, 2, 3 или 0", br);
            if ("1".equals(string)) {
                CategoryDTO categoryDTO = categoryService.addCategory(getString("Название категории", br), userDTO.getId());
                successOrError(categoryDTO != null);
            } else if ("2".equals(string)) {
                CategoryDTO categoryDTO = categoryService.editCategory(getLong(getString("Id категории", br)), getString("Новое название категории", br));
                successOrError(categoryDTO != null);
            } else if ("3".equals(string)) {
                successOrError(categoryService.deleteCategory(getLong(getString("Id категории", br))));
            } else if ("0".equals(string)) {
                break;
            }
        }
    }

    private void transactions(BufferedReader br) throws IOException {
        while (true) {
            menuTransaction();
            String string = getString("1 или 0", br);
            if ("1".equals(string)) {
                TransactionDTO transactionDTO = transactionService.addTransaction(
                        getString("Коментарий", br),
                        getLong(getString("Счет списания", br)),
                        getLong(getString("Счет зачисления", br)),
                        new BigDecimal(getString("Сумму", br)),
                        new Timestamp(new Date().getTime()),
                        Collections.emptyList()
                );
                successOrError(transactionDTO != null);
            } else if ("0".equals(string)) {
                break;
            }
        }
    }

    private void successOrError(boolean b) {
        System.out.println(b ? "Успех." : "Ошибка.");
    }

    @Override
    public void run(String... args) throws Exception {
        if (args.length > 0 && args[0].equals("-s")) {
            start();
        }
    }
}
