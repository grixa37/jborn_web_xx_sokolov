package ru.sokolov.web.controller;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.sokolov.dto.AccountDTO;
import ru.sokolov.entity.UserModel;
import ru.sokolov.repository.UserModelRepository;
import ru.sokolov.security.CustomUserDetails;
import ru.sokolov.service.AccountService;
import ru.sokolov.web.form.AccountForm;

import javax.validation.Valid;
import java.util.List;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Controller
public class AccountController {

    public static final String FETCH_ACCOUNTS = "/accounts";
    public static final String CREAT_ACCOUNT = "/accounts/account/create";
    public static final String DELETE_ACCOUNT = "/accounts/account/delete/{account_id}";
    public static final String REDIRECT = "redirect:";

    UserModelRepository userModelRepository;
    AccountService accountService;

    @GetMapping(FETCH_ACCOUNTS)
    public String getPersonal(Model model) {
        UserModel user = currentUser();
        List<AccountDTO> accounts = accountService.getAccounts(user.getId());
        if (accounts.isEmpty()) {
            model.addAttribute("list", accounts);
        }
        model.addAttribute("list", accounts);
        model.addAttribute("user", user);

        return "accounts";
    }

    @GetMapping(CREAT_ACCOUNT)
    public String getAddAccount(Model model, AccountForm form) {
        UserModel user = currentUser();
        model.addAttribute("user", user);
        model.addAttribute("addAccount", form);

        return "addAccount";
    }

    @PostMapping(CREAT_ACCOUNT)
    public String postAddAccount(@ModelAttribute("addAccount")
                                 @Valid AccountForm form,
                                 BindingResult result,
                                 Model model) {
        UserModel user = currentUser();
        if (!result.hasErrors()) {
            accountService.addAccount(form.getNameAccount(), user.getId(), form.getBalance());

            return REDIRECT + FETCH_ACCOUNTS;
        } else {
            model.addAttribute("addAccount", form);
            model.addAttribute("user", user);

            return "addAccount";
        }
    }

    @GetMapping(DELETE_ACCOUNT)
    public String postDelAccount(@PathVariable(value = "account_id") long accountId) {
        accountService.deleteAccount(accountId);

        return REDIRECT + FETCH_ACCOUNTS;
    }

    private UserModel currentUser() {
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();

        CustomUserDetails user = (CustomUserDetails) principal;

        return userModelRepository.getOne(user.getId());
    }
}
