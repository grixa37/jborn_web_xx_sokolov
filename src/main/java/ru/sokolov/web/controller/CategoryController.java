package ru.sokolov.web.controller;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import ru.sokolov.dto.CategoryDTO;
import ru.sokolov.entity.CategoryModel;
import ru.sokolov.entity.TransactionModel;
import ru.sokolov.entity.UserModel;
import ru.sokolov.exception.CustomException;
import ru.sokolov.repository.CategoryModelRepository;
import ru.sokolov.repository.TransactionModelRepository;
import ru.sokolov.repository.UserModelRepository;
import ru.sokolov.security.CustomUserDetails;
import ru.sokolov.service.CategoryService;
import ru.sokolov.web.form.CategoryForm;
import ru.sokolov.web.form.ReportsForm;
import ru.sokolov.web.form.ReportsFormView;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Controller
public class CategoryController {
    TransactionModelRepository transactionModelRepository;
    CategoryModelRepository categoryModelRepository;
    UserModelRepository userModelRepository;
    CategoryService categoryService;

    public static final String FETCH_CATEGORIES = "/categories";
    public static final String CREAT_CATEGORY = "/categories/category/creat";
    public static final String UPDATE_CATEGORY = "/categories/category/update/{category_id}";
    public static final String DELETE_CATEGORY = "/categories/category/delete/{category_id}";
    public static final String CREAT_REPORT = "/categories/report/creat";
    public static final String REDIRECT = "redirect:";

    @GetMapping(FETCH_CATEGORIES)
    public String getCategory(Model model) {
        UserModel user = currentUser();
        List<CategoryDTO> categories = categoryService.getCategories(user.getId());
        model.addAttribute("user", user);
        model.addAttribute("list", categories);

        return "category";
    }

    @GetMapping(CREAT_REPORT)
    public String getReports(Model model, ReportsForm form) {
        UserModel user = currentUser();
        mapCategoriesIdName(model, user);
        model.addAttribute("user", user);
        model.addAttribute("reports", form);

        return "getDataReports";
    }

    @PostMapping(CREAT_REPORT)
    public String postReports(@ModelAttribute("reports")
                              @Valid ReportsForm reportsForm,
                              BindingResult result,
                              Model model) {
        UserModel user = currentUser();
        if (!result.hasErrors()) {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-M-dd");
            List<TransactionModel> transactions;
            try {
                Timestamp before = new Timestamp(formatter.parse(reportsForm.getDateBefore()).getTime() + 24 * 60 * 60 * 1000 - 1);
                Timestamp after = new Timestamp(formatter.parse(reportsForm.getDateAfter()).getTime());
                transactions = transactionModelRepository.findDistinctByCategoriesInAndDateAfterAndDateBeforeOrderById(
                        reportsForm.getCategories(),
                        after,
                        before

                );
            } catch (ParseException e) {
                throw new CustomException(e);
            }
            List<ReportsFormView> reports = new ArrayList<>();
            transactions.forEach(transaction -> {
                List<CategoryModel> categories = transaction.getCategories();
                ReportsFormView reportsFormView = new ReportsFormView();
                String listCategories = reportsForm.getCategories()
                        .stream()
                        .filter(categories::contains)
                        .map(category -> category.getNameCategory() + " ")
                        .collect(Collectors.joining());
                reportsFormView.setCategoriesName(listCategories.trim());
                reportsFormView.setTransactionModel(transaction);
                reports.add(reportsFormView);
            });

            model.addAttribute("list1", reports);
            model.addAttribute("user", user);

            return "reports";
        } else {
            mapCategoriesIdName(model, user);
            model.addAttribute("reports", reportsForm);
            model.addAttribute("user", user);

            return "getDataReports";
        }
    }

    @GetMapping(CREAT_CATEGORY)
    public String getAddCategory(Model model, CategoryForm form) {
        UserModel user = currentUser();
        model.addAttribute("form", form);
        model.addAttribute("user", user);

        return "addCategory";
    }

    @PostMapping(CREAT_CATEGORY)
    public String postAddCategory(@ModelAttribute("form")
                                  @Valid CategoryForm form,
                                  BindingResult result,
                                  Model model) {
        UserModel user = currentUser();
        if (!result.hasErrors()) {
            categoryModelRepository.saveAndFlush(
                    CategoryModel.createDefault(form.getNameCategory(), user)
            );

            return REDIRECT + FETCH_CATEGORIES;
        } else {
            model.addAttribute("form", form);
            model.addAttribute("user", user);

            return "addCategory";
        }
    }

    @GetMapping(UPDATE_CATEGORY)
    public String getEditCategory(@PathVariable(value = "category_id") long categoryId,
                                  Model model,
                                  CategoryForm form) {
        UserModel user = currentUser();
        CategoryModel category = categoryModelRepository.getOne(categoryId);
        form.setId(category.getId());
        form.setNameCategory(category.getNameCategory());
        model.addAttribute("category", form);
        model.addAttribute("user", user);

        return "editCategory";
    }

    @PostMapping(UPDATE_CATEGORY)
    public String postEditCategory(@ModelAttribute("category")
                                   @Valid CategoryForm categoryForm,
                                   BindingResult result,
                                   @PathVariable(value = "category_id") long categoryId,
                                   Model model) {
        UserModel user = currentUser();
        if (!result.hasErrors()) {
            CategoryModel category = categoryModelRepository.getOne(categoryId);
            category.setNameCategory(categoryForm.getNameCategory());
            categoryModelRepository.save(category);

            return REDIRECT + FETCH_CATEGORIES;
        } else {
            categoryForm.setId(categoryId);
            mapCategoriesIdName(model, user);
            model.addAttribute("category", categoryForm);
            model.addAttribute("user", user);

            return "editCategory";
        }
    }

    @GetMapping(DELETE_CATEGORY)
    public String postDelCategory(@PathVariable(value = "category_id") long categoryId) {
        CategoryModel category = categoryModelRepository.getOne(categoryId);
        categoryModelRepository.delete(category);

        return REDIRECT + FETCH_CATEGORIES;
    }

    private void mapCategoriesIdName(Model model, UserModel user) {
        List<CategoryModel> categories = categoryModelRepository.findAllByUserOrderById(user);
        Map<String, String> mapCategoriesIdName = categories.stream()
                .collect(Collectors.toMap(
                        category -> category.getId().toString(),
                        CategoryModel::getNameCategory, (a, b) -> b)
                );
        model.addAttribute("list", mapCategoriesIdName);
    }

    private UserModel currentUser() {
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        CustomUserDetails user = (CustomUserDetails) principal;

        return userModelRepository.getOne(user.getId());
    }
}
