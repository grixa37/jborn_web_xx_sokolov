package ru.sokolov.web.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@RequiredArgsConstructor
@Controller
public class LoginController {

    @GetMapping("/")
    public String index() {

        return "redirect:/accounts";
    }

    @GetMapping("/login")
    public String getLogin() {
        return "login";
    }
}
