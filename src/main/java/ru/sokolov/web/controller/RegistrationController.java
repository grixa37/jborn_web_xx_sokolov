package ru.sokolov.web.controller;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.sokolov.entity.UserModel;
import ru.sokolov.repository.UserModelRepository;
import ru.sokolov.security.UserRole;
import ru.sokolov.web.form.UserForm;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.Collections;
import java.util.HashSet;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Controller
public class RegistrationController {
    UserModelRepository userModelRepository;
    PasswordEncoder passwordEncoder;

    public static final String REGISTRATION  = "/registration";
    public static final String REDIRECT  = "redirect:";

    @GetMapping(REGISTRATION)
    public String getRegistration(Model model, UserForm form) {
        model.addAttribute("form", form);

        return "registration";
    }

    @PostMapping(REGISTRATION)
    public String postRegistration(@ModelAttribute("form")
                                   @Valid UserForm userForm,
                                   BindingResult result,
                                   Model model,
                                   HttpServletRequest httpServletRequest) {
        if (!result.hasErrors()) {
            UserModel userModelByEmail = userModelRepository.findByEmail(userForm.getEmail());
            if (userModelByEmail == null) {
                UserModel user = new UserModel()
                        .setName(userForm.getName())
                        .setLastName(userForm.getLastName())
                        .setEmail(userForm.getEmail())
                        .setPassword(passwordEncoder.encode(userForm.getPassword()))
                        .setRoles(new HashSet<>(Collections.singleton(UserRole.USER)));
                userModelRepository.save(user);
                HttpSession session = httpServletRequest.getSession();
                session.setAttribute("user", user);

                return REDIRECT + "/accounts";
            } else {
                result.addError(new FieldError(
                        "form",
                        "email",
                        "Такой пользователь уже существует"));
            }
        }
        model.addAttribute("form", userForm);

        return REGISTRATION;
    }
}


