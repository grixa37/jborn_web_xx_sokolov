package ru.sokolov.web.controller;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import ru.sokolov.entity.AccountModel;
import ru.sokolov.entity.CategoryModel;
import ru.sokolov.entity.UserModel;
import ru.sokolov.repository.AccountModelRepository;
import ru.sokolov.repository.CategoryModelRepository;
import ru.sokolov.repository.UserModelRepository;
import ru.sokolov.security.CustomUserDetails;
import ru.sokolov.service.TransactionService;
import ru.sokolov.web.form.TransactionForm;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
@Controller
public class TransactionController {
    TransactionService transactionService;
    AccountModelRepository accountModelRepository;
    UserModelRepository userModelRepository;
    CategoryModelRepository categoryModelRepository;

    public static final String CREAT_TRANSACTION = "/transactions/transaction/creat";
    public static final String REDIRECT  = "redirect:";

    @GetMapping(CREAT_TRANSACTION)
    public String getAddTransaction(Model model, TransactionForm form) {
        UserModel user = currentUser();
        mapAccountsIdName(model, user);
        mapCategoriesIdName(model, user);
        model.addAttribute("addTransaction", form);
        model.addAttribute("user", user);

        return "transaction";
    }

    @PostMapping(CREAT_TRANSACTION)
    public String postAddTransaction(@ModelAttribute("addTransaction")
                                     @Valid TransactionForm transactionForm,
                                     BindingResult result,
                                     Model model) {
        UserModel user = currentUser();
        if (!result.hasErrors()) {
            transactionService.addTransaction(
                    transactionForm.getComment(),
                    transactionForm.getFromAccountId(),
                    transactionForm.getToAccountId(),
                    transactionForm.getAmount(),
                    Timestamp.valueOf(LocalDateTime.now()),
                    transactionForm.getCategoriesId()
            );

            return REDIRECT + CREAT_TRANSACTION;
        } else {
            mapAccountsIdName(model, user);
            mapCategoriesIdName(model, user);
            model.addAttribute("addTransaction", transactionForm);
            model.addAttribute("user", user);

            return "transaction";
        }
    }

    private void mapAccountsIdName(Model model, UserModel user) {
        List<AccountModel> accounts = accountModelRepository
                .findAllByUserOrderById(user)
                .orElse(Collections.emptyList());
        Map<String, String> mapAccountsIdName = accounts.stream()
                .collect(Collectors.toMap(
                        account -> account.getId().toString(),
                        AccountModel::getNameAccount, (a, b) -> b)
                );
        mapAccountsIdName.put("", "пусто");
        model.addAttribute("list", mapAccountsIdName);
    }

    private void mapCategoriesIdName(Model model, UserModel user) {
        List<CategoryModel> categories = categoryModelRepository.findAllByUserOrderById(user);
        Map<String, String> mapCategoriesIdName = categories.stream()
                .collect(Collectors.toMap(
                        category -> category.getId().toString(),
                        CategoryModel::getNameCategory, (a, b) -> b)
                );
        model.addAttribute("categories", mapCategoriesIdName);
    }

    private UserModel currentUser() {
        Object principal = SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        if (principal instanceof CustomUserDetails) {
            CustomUserDetails user = (CustomUserDetails) principal;

            return userModelRepository.getOne(user.getId());
        }

        return null;
    }
}
