package ru.sokolov.web.form;

import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class AccountForm {

    Long id;

    @NotBlank(message = "Поле не может быть пустым!")
    String nameAccount;

    @NotNull(message = "Поле не может быть пустым!")
    BigDecimal balance;
}
