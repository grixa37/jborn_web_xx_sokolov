package ru.sokolov.web.form;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import ru.sokolov.entity.TransactionModel;

import javax.validation.constraints.NotBlank;

@Data
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class CategoryForm {

    Long id;

    @NotBlank(message = "Поле не может быть пустым!")
    String nameCategory;

    TransactionModel transactionModel;
}
