package ru.sokolov.web.form;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.sokolov.entity.CategoryModel;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@Accessors(chain = true)
public class ReportsForm {

    private Long id;

    @NotEmpty(message = "Выберите хотя бы одну категорию!")
    private List<CategoryModel> categories;

    @NotBlank(message = "Выберите дату!")
    private String dateBefore;

    @NotBlank(message = "Выберите дату!")
    private String dateAfter;
}
