package ru.sokolov.web.form;

import lombok.Data;
import lombok.experimental.Accessors;
import ru.sokolov.entity.CategoryModel;
import ru.sokolov.entity.TransactionModel;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@Accessors(chain = true)
public class ReportsFormView {

    String categoriesName;

    TransactionModel transactionModel;
}
