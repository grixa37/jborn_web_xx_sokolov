package ru.sokolov.web.form;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.FieldDefaults;
import ru.sokolov.entity.CategoryModel;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TransactionForm {

    String comment;

    Long fromAccountId;

    Long toAccountId;

    @NotNull(message = "Поле не может быть пустым!")
    @Min(value = 0, message = "сумма должна быть больше чем 0!")
    @Max(value = 10000000, message = "превышен лимит!")
    BigDecimal amount;

    @NotEmpty(message = "Выберите хотя бы одну категорию!")
    List<Long> categoriesId;
}
