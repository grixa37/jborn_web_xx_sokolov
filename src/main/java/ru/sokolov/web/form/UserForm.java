package ru.sokolov.web.form;

import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
public class UserForm {

    private String name;

    private String lastName;

    @Email
    @NotBlank(message = "Поле не может быть пустым!")
    private String email;

    @NotBlank(message = "Поле не может быть пустым!")
    private String password;
}
