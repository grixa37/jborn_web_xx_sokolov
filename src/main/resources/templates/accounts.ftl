<#import "/spring.ftl" as spring />

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>assistant</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span2">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="/accounts" methods="get">Счета</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/transactions/transaction/creat" methods="get">Транзакции</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/categories">Категории</a>
                            </li>
                        </ul>
                    </div>
                    <div class="span10">
                        <table class="table">
                            <tr>
                                <th>Название счета</th>
                                <th>Баланс</th>
                                <th>Действие</th>
                            </tr>
                            <#list list as account>
                                <tr>
                                    <td>${account.getNameAccount()}</td>
                                    <td>${account.getBalance()}</td>
                                    <td><a class="btn btn-outline-danger btn-sm"
                                           href="/accounts/account/delete/${account.getId()}">удалить</a></td>
                                </tr>
                            </#list>
                        </table>
                    </div>
                    <div class="span10">
                        <table class="row">
                            <tr>
                                <td><a href="/accounts/account/create" methods="get"
                                       class="btn btn-outline-success btn-sm">добавить</a></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div><h2>${user.email}</h2></div>
            <a class="btn btn-primary" href="/logout" role="button">Выход</a>
        </div>
    </div>
</div>
<script src="/js/bootstrap.min.js"></script>
</body>
</html>