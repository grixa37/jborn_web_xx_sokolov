<#import "/spring.ftl" as spring />

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>assistant</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span2">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="/accounts" methods="get">Счета</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/transactions/transaction/creat" methods="get">Транзакции</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/categories">Категории</a>
                            </li>
                        </ul>
                    </div>
                    <div class="span10">
                        <form action="/accounts/account/create" method="post">
                            <div class="mb-3">
                                <h2>Добавить счет</h2><br>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputNameAccount" class="form-label">Название</label>
                                <@spring.formInput "addAccount.nameAccount" "class=\"form-control\" id=\"exampleInputNameAccount\" place holder\"Enter NameAccount\"" "nameAccount" />
                                <@spring.showErrors "<br>" "color: red"/>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputBalance" class="form-label">Баланс</label>
                                <@spring.formInput "addAccount.balance" "class=\"form-control\" id=\"exampleInputBalance\" place holder\"Enter Balance\"" "balance" />
                                <@spring.showErrors "<br>" "color: red"/>
                            </div>
                            <div class="mb-3">
                                <br>
                                <button type="submit" class="btn btn-primary">Добавить</button>
                                <a class="btn btn-primary" href="/accounts" role="button">Отмена</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div><h2>${user.email}</h2></div>
            <a class="btn btn-primary" href="/logout" role="button">Выход</a>
        </div>
    </div>
</div>
<script src="/js/bootstrap.min.js"></script>
</body>
</html>