<#import "/spring.ftl" as spring />

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>assistant</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span2">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link" href="/accounts" methods="get">Счета</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/transactions/transaction/creat" methods="get">Транзакции</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="/categories">Категории</a>
                            </li>
                        </ul>
                    </div>
                    <div class="span10">
                        <table class="table">
                            <tr>
                                <th>Категория</th>
                                <th>Действие</th>
                            </tr>
                            <#list list as category>
                                <tr>
                                    <td>${category.nameCategory}</td>
                                    <td><a class="btn btn-outline-success btn-sm"
                                           href="/categories/category/update/${category.getId()}">редактировать</a>
                                        <a class="btn btn-outline-danger btn-sm"
                                           href="/categories/category/delete/${category.getId()}">удалить</a>
                                    </td>
                                </tr>
                            </#list>
                        </table>
                    </div>
                    <div class="span10">
                        <table class="row">
                            <tr>
                                <td><a href="/categories/category/creat"
                                       class="btn btn-outline-success btn-sm">добавить</a></td>
                                <td><a href="/categories/report/creat"
                                       class="btn btn-outline-success btn-sm">отчет</a></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div><h2>${user.email}</h2></div>
            <a class="btn btn-primary" href="/logout" role="button">Выход</a>
        </div>
    </div>
</div>
<script src="/js/bootstrap.min.js"></script>
</body>
</html>