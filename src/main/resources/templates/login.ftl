<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>assistant</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <form action="/perform_login" method="post">
                <div class="mb-3">
                    <h2>Финансовый помощник</h2><br>
                    <label for="exampleInputEmail" class="form-label">Электронная почта</label>
                    <input name="email" class="form-control" id="exampleInputEmail" placeholder="Enter Email"
                           type="text">
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword" class="form-label">Пароль</label>
                    <input name="password" class="form-control" id="exampleInputPassword" placeholder="Enter Password"
                           type="text">
                </div>
                <div class="mb-3">
                    <br>
                    <button type="submit" class="btn btn-primary">Вход</button>
                    <a class="btn btn-link" href="/registration" methods="post" role="button">Регистрация</a>
                </div>
            </form>
        </div>
        <div class="col-md-3"></div>
    </div>
</div>
<script src="/js/bootstrap.min.js"></script>
</body>
</html>