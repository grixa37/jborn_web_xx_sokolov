<#import "/spring.ftl" as spring />

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>assistant</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col"></div>
        <div class="col">
            <form action="/registration" method="post">
                <div class="mb-3">
                    <h2>Финансовый помощник</h2><br>
                </div>
                <div class="mb-3">
                    <label for="exampleInputName" class="form-label">Имя</label>
                    <@spring.formInput "form.name" "class=\"form-control\" id=\"exampleInputName\" place holder\"Enter Name\"" "name" />
                </div>
                <div class="mb-3">
                    <label for="exampleInputLastName" class="form-label">Фамилия</label>
                    <@spring.formInput "form.lastName" "class=\"form-control\" id=\"exampleInputLastName\" place holder\"Enter LastName\"" "lastName" />
                </div>
                <div class="mb-3">
                    <label for="exampleInputEmail" class="form-label">Электронная почта</label>
                    <@spring.formInput "form.email" "class=\"form-control\" id=\"exampleInputEmail\" place holder\"Enter Email\"" "email" />
                    <@spring.showErrors "<br>" "color: red"/>
                </div>
                <div class="mb-3">
                    <label for="exampleInputPassword" class="form-label">Пароль</label>
                    <@spring.formInput "form.password" "class=\"form-control\" id=\"exampleInputPassword\" place holder\"Password\"" "password" />
                    <@spring.showErrors "<br>" "color: red"/>
                </div>
                <div class="mb-3">
                    <br>
                    <button type="submit" class="btn btn-primary">Регистрация</button>
                    <a class="btn btn-primary" href="/logout" role="button">Отмена</a>
                </div>
            </form>
        </div>
        <div class="col"></div>
    </div>
</div>
<script src="/js/bootstrap.min.js"></script>
</body>
</html>