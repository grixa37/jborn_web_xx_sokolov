<#import "/spring.ftl" as spring />

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>assistant</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span2">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link" href="/accounts" methods="get">Счета</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" aria-current="page" href="/transactions/transaction/creat"
                                   methods="get">Транзакции</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="/categories">Категории</a>
                            </li>
                        </ul>
                    </div>
                    <div class="span10">
                        <table class="table">
                            <tr>
                                <th>Дата</th>
                                <th>Категории</th>
                                <th>Счет списания</th>
                                <th>Счет зачисления</th>
                                <th>Сумма</th>
                                <th>Коментарий</th>
                            </tr>
                            <#list list1 as report>
                                <tr>
                                    <td>${report.getTransactionModel().getDate()}</td>
                                    <td>${report.getCategoriesName()}</td>
                                    <td><#if report.transactionModel.getFromAccount()?has_content>
                                            ${report.transactionModel.getFromAccount().getNameAccount()}
                                        </#if>
                                    </td>
                                    <td>
                                        <#if report.transactionModel.getToAccount()?has_content>
                                        ${report.transactionModel.getToAccount().getNameAccount()}
                                        </#if>
                                    </td>
                                    <td>${report.transactionModel.amount}</td>
                                    <td>${report.transactionModel.comment}</td>
                                </tr>
                            </#list>
                        </table>
                    </div>
                    <div class="span10">
                        <a class="btn btn-primary" href="/categories" role="button">Назад</a>
                    </div>
                </div>
            </div>

        </div>
        <div class="col-md-3">
            <div><h2>${user.email}</h2></div>
            <a class="btn btn-primary" href="/logout" role="button">Выход</a>
        </div>
    </div>
</div>
<script src="/js/bootstrap.min.js"></script>
</body>
</html>