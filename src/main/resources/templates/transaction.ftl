<#import "/spring.ftl" as spring />

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>assistant</title>
    <link href="/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="container-fluid">
                <div class="row-fluid">
                    <div class="span2">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link" href="/accounts" methods="get">Счета</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="/transactions/transaction/creat" methods="get">Транзакции</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/categories">Категории</a>
                            </li>
                        </ul>
                    </div>
                    <div class="span10">
                        <form action="/transactions/transaction/creat" method="post">
                            <div class="mb-3">
                                <h2>Добавить транзакцию</h2><br>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputComment" class="form-label">Коментарий</label>
                                <@spring.formInput "addTransaction.comment" "class=\"form-control\" id=\"exampleInputName\" place holder\"Enter Сomment\"" "comment" />
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputFromAccount" class="form-label">Счет списания</label>
                                <@spring.formSingleSelect "addTransaction.fromAccountId" list "class=\"form-control\" id=\"exampleInputFromAccountId\" place holder\"Enter FromAccountId\" \"fromAccountId\" "/>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputToAccount" class="form-label">Счет зачисления</label>
                                <@spring.formSingleSelect "addTransaction.toAccountId" list "class=\"form-control\" id=\"exampleInputToAccountId\" place holder\"Enter ToAccountId\" \"toAccountId\" " />
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputCategories" class="form-label">Категории</label><br>
                                <@spring.formCheckboxes "addTransaction.categoriesId" categories "<br>" "id=\"exampleInputCategories\" \"categories\""/>
                                <@spring.showErrors "<br>" "color: red"/>
                            </div>
                            <div class="mb-3">
                                <label for="exampleInputAmount" class="form-label">Сумма</label>
                                <@spring.formInput "addTransaction.amount" "class=\"form-control\" id=\"exampleInputAmount\" place holder\"Enter Amount\"" "amount" />
                                <@spring.showErrors "<br>" "color: red"/>
                            </div>
                            <div class="mb-3">
                                <br>
                                <button type="submit" class="btn btn-primary">Добавить</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div><h2>${user.email}</h2></div>
            <a class="btn btn-primary" href="/logout" role="button">Выход</a>
        </div>
    </div>
</div>
<script src="static/js/bootstrap.min.js"></script>
</body>
</html>