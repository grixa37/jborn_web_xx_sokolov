package ru.sokolov.api.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.sokolov.api.converter.AccountDtoToResponseConverter;
import ru.sokolov.config.SecurityConfiguration;
import ru.sokolov.entity.UserModel;
import ru.sokolov.repository.UserModelRepository;
import ru.sokolov.security.UserRole;
import ru.sokolov.dto.AccountDTO;
import ru.sokolov.service.AccountService;
import ru.sokolov.web.MockSecurityConfiguration;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(AccountControllerApi.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
public class AccountControllerApiTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserModelRepository userModelRepository;

    @MockBean
    AccountService accountService;

    @SpyBean
    AccountDtoToResponseConverter converter;

    @Before
    public void setUp() {
        when(userModelRepository.getOne(1L)).thenReturn(getUser());
    }

    @Test
    public void getAccounts_Auth() throws Exception {
        List<AccountDTO> accountDTOList = new ArrayList<>();
        accountDTOList.add(
                new AccountDTO()
                        .setId(1L)
                        .setNameAccount("TestAccount")
                        .setUserId(1L)
                        .setBalance(new BigDecimal(10000))
        );
        when(accountService.getAccounts(1L)).thenReturn(accountDTOList);
        mockMvc.perform(get("/api/accounts"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "  \"accounts\":" +
                        " [\n" +
                        "    {\n" +
                        "      \"id\": 1,\n" +
                        "      \"nameAccount\": \"TestAccount\",\n" +
                        "      \"userId\": 1,\n" +
                        "      \"balance\": 10000\n" +
                        "    }\n" +
                        "  ]\n" +
                        "}"));
    }

    @Test
    public void addAccount_Auth() throws Exception {
        when(accountService.addAccount(
                "TestAccount",
                1L, new BigDecimal(10000)))
                .thenReturn(
                        new AccountDTO()
                                .setId(1L)
                                .setNameAccount("TestAccount")
                                .setUserId(1L)
                                .setBalance(new BigDecimal(10000))
                );
        mockMvc.perform(post("/api/accounts/account")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"nameAccount\":" +
                                "  \"TestAccount\",\n" +
                                "  \"balance\": 10000\n" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "  \"id\":" +
                        "  1,\n" +
                        "  \"nameAccount\": \"TestAccount\",\n" +
                        "  \"userId\": 1,\n" +
                        "  \"balance\": 10000\n" +
                        "}"));
    }

    @Test
    public void delAccount_Auth_Ok() throws Exception {
        when(accountService.deleteAccount(1L)).thenReturn(true);
        mockMvc.perform(delete("/api/accounts/account/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"id\":" +
                                "  1\n" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "  \"response\": \"Success!\"" +
                        "\n" +
                        "}"));
    }

    @Test
    public void delAccount_Auth_NotOk() throws Exception {
        when(accountService.deleteAccount(1L)).thenReturn(false);
        mockMvc.perform(delete("/api/accounts/account/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"id\":" +
                                "  1\n" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "  \"response\": \"Not success!\"" +
                        "\n" +
                        "}"));
    }

    private UserModel getUser() {
        return new UserModel()
                .setId(1L)
                .setEmail("email@gmail.com")
                .setPassword("some-password")
                .setRoles(Collections.singleton(UserRole.USER));
    }
}