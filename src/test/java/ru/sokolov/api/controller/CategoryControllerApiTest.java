package ru.sokolov.api.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.sokolov.api.converter.CategoryDtoToResponseConverter;
import ru.sokolov.config.SecurityConfiguration;
import ru.sokolov.entity.UserModel;
import ru.sokolov.repository.UserModelRepository;
import ru.sokolov.security.UserRole;
import ru.sokolov.dto.CategoryDTO;
import ru.sokolov.service.CategoryService;
import ru.sokolov.web.MockSecurityConfiguration;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(CategoryControllerApi.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
public class CategoryControllerApiTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    CategoryService categoryService;

    @MockBean
    UserModelRepository userModelRepository;

    @SpyBean
    CategoryDtoToResponseConverter converter;

    @Before
    public void setUp() {
        when(userModelRepository.getOne(1L)).thenReturn(getUser());
    }

    @Test
    public void addCategory_Auth() throws Exception {
        when(categoryService.addCategory("TestCategory", 1L))
                .thenReturn(
                        new CategoryDTO()
                                .setId(1L).setUserId(1L)
                                .setNameCategory("TestCategory")
                );
        mockMvc.perform(post("/api/categories")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"categoryName\": \"TestCategory\"\n" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "  \"id\": 1,\n" +
                        "  \"userId\": 1,\n" +
                        "  \"nameCategory\": \"TestCategory\"\n" +
                        "}"));
    }

    @Test
    public void editCategory_Auth() throws Exception {
        when(categoryService.editCategory(1L, "NewTestCategory"))
                .thenReturn(
                        new CategoryDTO()
                                .setId(1L)
                                .setUserId(1L)
                                .setNameCategory("NewTestCategory"));
        mockMvc.perform(put("/api/categories")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"id\": 1,\n" +
                                "  \"nameCategory\": \"NewTestCategory\"\n" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "  \"id\":" +
                        " 1,\n" +
                        "  \"userId\": 1,\n" +
                        "  \"nameCategory\": \"NewTestCategory\"\n" +
                        "}"));
    }

    @Test
    public void delCategory_Auth_Ok() throws Exception {
        when(categoryService.deleteCategory(1L)).thenReturn(true);
        mockMvc.perform(delete("/api/categories/category/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"id\": 1\n" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "  \"response\": \"Success!\"\n" +
                        "}"));
    }

    @Test
    public void delCategory_Auth_NotOk() throws Exception {
        when(categoryService.deleteCategory(1L)).thenReturn(false);
        mockMvc.perform(delete("/api/categories/category/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"id\": 1\n" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "  \"response\": \"Not success!\"\n" +
                        "}"));
    }

    @Test
    public void getReports_Auth_Found() throws Exception {
        List<String> array = new ArrayList<>();
        array.add("Test");
        when(categoryService.reports(1L, new Timestamp(1602288000000L), new Timestamp(1605052800000L))).thenReturn(array);
        mockMvc.perform(post("/api/reports")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"id\": 1,\n" +
                                "  \"timestampFrom\": \"2020-10-10\",\n" +
                                "  \"timestampTo\": \"2020-11-11\"\n" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "  \"reports\":" +
                        " [\n" +
                        "      \"Test\"" +
                        "\n" +
                        "  ]\n" +
                        "}"));
    }

    @Test
    public void getReports_Auth_NotFound() throws Exception {
        List<String> array = new ArrayList<>();
        array.add("Test");
        when(categoryService.reports(1L, new Timestamp(160228800000L), new Timestamp(1605052800000L))).thenReturn(array);
        mockMvc.perform(post("/api/reports")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"id\": 1,\n" +
                                "  \"timestampFrom\": \"2020-10-10\",\n" +
                                "  \"timestampTo\": \"2020-11-11\"\n" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "  \"reports\":" +
                        " [\n" +
                        "      \n" +
                        "  ]\n" +
                        "}"));
    }

    private UserModel getUser() {
        return new UserModel()
                .setId(1L)
                .setEmail("email@gmail.com")
                .setPassword("some-password")
                .setRoles(Collections.singleton(UserRole.USER));
    }
}