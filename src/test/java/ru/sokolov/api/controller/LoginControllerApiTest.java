package ru.sokolov.api.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.sokolov.api.converter.UserDtoToResponseConverter;
import ru.sokolov.config.SecurityConfiguration;
import ru.sokolov.service.AuthService;
import ru.sokolov.service.UserDTO;
import ru.sokolov.web.MockSecurityConfiguration;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(LoginControllerApi.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
public class LoginControllerApiTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    AuthService authService;

    @SpyBean
    UserDtoToResponseConverter converter;

    @Before
    public void setUp() {
    }

    @Test
    public void registration() throws Exception {
        when(authService.registration("testName",
                "testLastName",
                "emaill@gmail.com",
                "password"))
                .thenReturn(
                        new UserDTO()
                                .setId(1L)
                                .setName("testName")
                                .setLastName("testLastName")
                                .setEmail("emaill@gmail.com")
                );
        mockMvc.perform(post("/api/registration")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"name\": \"testName\",\n" +
                                "  \"lastName\": \"testLastName\",\n" +
                                "  \"email\": \"emaill@gmail.com\",\n" +
                                "  \"password\": \"password\"\n" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "  \"id\": 1,\n" +
                        "  \"name\": \"testName\",\n" +
                        "  \"lastName\": \"testLastName\",\n" +
                        "  \"email\": \"emaill@gmail.com\"\n" +
                        "}"));
    }
}