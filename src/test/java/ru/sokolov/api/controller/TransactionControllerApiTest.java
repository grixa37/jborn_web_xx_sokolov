package ru.sokolov.api.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.sokolov.api.converter.TransactionDtoToResponseConverter;
import ru.sokolov.config.SecurityConfiguration;
import ru.sokolov.entity.UserModel;
import ru.sokolov.repository.AccountModelRepository;
import ru.sokolov.repository.UserModelRepository;
import ru.sokolov.security.UserRole;
import ru.sokolov.service.TransactionDTO;
import ru.sokolov.service.TransactionService;
import ru.sokolov.web.MockSecurityConfiguration;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collections;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TransactionControllerApi.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
public class TransactionControllerApiTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    TransactionService transactionService;

    @MockBean
    AccountModelRepository accountModelRepository;

    @MockBean
    UserModelRepository userModelRepository;

    @SpyBean
    TransactionDtoToResponseConverter converter;

    @Before
    public void setUp() {
        when(userModelRepository.getOne(1L)).thenReturn(getUser());
    }

    @Test
    public void addTransaction() throws Exception {
        BigDecimal bigDecimal = new BigDecimal(100);
        Timestamp timestamp = new Timestamp(1602288000000L);
        TransactionDTO transactionDTO = new TransactionDTO()
                .setId(1L)
                .setComment("TestComment")
                .setFromAccountId(1L)
                .setToAccountId(2L)
                .setAmount(bigDecimal)
                .setTimestamp(timestamp)
                .setCategoriesId(Collections.singletonList(1L));

        when(transactionService.addTransaction(
                eq("TestComment"),
                eq(1L),
                eq(2L),
                eq(bigDecimal),
                any(),
                eq(Collections.singletonList(1L)))
        )
                .thenReturn(transactionDTO);
        mockMvc.perform(post("/api/transactions")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\n" +
                                "  \"comment\": \"TestComment\",\n" +
                                "  \"fromAccountId\": 1,\n" +
                                "  \"toAccountId\": 2,\n" +
                                "  \"amount\": 100,\n" +
                                "  \"categoriesId\" : [\n" +
                                "    1\n" +
                                "  ]\n" +
                                "}"))
                .andExpect(status().isOk())
                .andExpect(content().json("{\n" +
                        "  \"id\": 1,\n" +
                        "  \"comment\": \"TestComment\",\n" +
                        "  \"fromAccountId\": 1,\n" +
                        "  \"toAccountId\": 2,\n" +
                        "  \"amount\": 100,\n" +
                        "  \"timestamp\": \"2020-10-10T00:00:00.000+00:00\",\n" +
                        "  \"categoriesId\" : [\n" +
                        "    1\n" +
                        "  ]\n" +
                        "}"));
    }

    private UserModel getUser() {
        return new UserModel()
                .setId(1L)
                .setEmail("email@gmail.com")
                .setPassword("some-password")
                .setRoles(Collections.singleton(UserRole.USER));
    }
}