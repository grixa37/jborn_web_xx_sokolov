package ru.sokolov.repository;

import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.sokolov.WebApplication;
import ru.sokolov.entity.UserModel;

import javax.persistence.EntityManager;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = WebApplication.class)
public class UserModelRepositoryTest {

    @Autowired
    UserModelRepository subj;

    @Autowired
    EntityManager em;

    @BeforeEach
    void setUp() {
    }

    @Test
    public void findByEmail() {
        UserModel user = subj.findByEmail("email@gmail.com");

        assertNotNull(user);
        assertEquals(1L, user.getId());
        assertEquals("email@gmail.com", user.getEmail());
        assertEquals("$2a$10$iU1GUPqxI3vfoIAL9uB1gOGuVfu7NdQOTtis6T3.LxoJlkRQMR7ca", user.getPassword());
    }
}