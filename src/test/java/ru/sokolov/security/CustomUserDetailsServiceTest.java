package ru.sokolov.security;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@SpringBootTest
@RunWith(SpringRunner.class)
public class CustomUserDetailsServiceTest {

    @Autowired
    CustomUserDetailsService subj;

    @Test
    public void loadUserByUsername() {
        UserDetails userDetails = subj.loadUserByUsername("email@gmail.com");

        assertNotNull(userDetails);
        assertEquals("email@gmail.com", userDetails.getUsername());
        assertEquals("$2a$10$iU1GUPqxI3vfoIAL9uB1gOGuVfu7NdQOTtis6T3.LxoJlkRQMR7ca", userDetails.getPassword());
        assertEquals(1, userDetails.getAuthorities().size());
        assertEquals(new CustomGrantedAuthority(UserRole.USER), userDetails.getAuthorities().iterator().next());


    }
}