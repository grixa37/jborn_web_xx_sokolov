package ru.sokolov.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.sokolov.api.converter.AccountModelToAccountDTOConvert;
import ru.sokolov.dto.AccountDTO;
import ru.sokolov.entity.AccountModel;
import ru.sokolov.entity.UserModel;
import ru.sokolov.repository.AccountModelRepository;
import ru.sokolov.repository.UserModelRepository;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {
    @InjectMocks
    AccountService subj;
    @Mock
    UserModelRepository userModelRepository;
    @Mock
    AccountModelRepository accountModelRepository;
    @Mock
    AccountModelToAccountDTOConvert accountModelToAccountDTOConvert;

    @Test
    public void getAccounts_empty() {
        UserModel user = userModelRepository.getOne(1L);
        List<AccountDTO> accounts = subj.getAccounts(1L);
        assertTrue(accounts.isEmpty());
        verify(accountModelRepository, times(1)).findAllByUserOrderById(user);
    }

    @Test
    public void getAccounts_notEmpty() {
        UserModel user = userModelRepository.getOne(1L);
        List<AccountModel> accountsModel = new ArrayList<>();
        AccountModel accountModel = new AccountModel();
        accountModel.setId(1L);
        accountModel.setNameAccount("name");
        accountModel.setUser(new UserModel().setId(1L));
        accountModel.setBalance(new BigDecimal("123"));
        accountsModel.add(accountModel);
        when(accountModelRepository.findAllByUserOrderById(user)).thenReturn(java.util.Optional.of(accountsModel));

        AccountDTO accountDTO = new AccountDTO();
        accountDTO.setId(1L);
        accountDTO.setNameAccount("name");
        accountDTO.setUserId(1L);
        accountDTO.setBalance(new BigDecimal("123"));

        when(accountModelToAccountDTOConvert.convert(accountModel)).thenReturn(accountDTO);

        List<AccountDTO> accounts = subj.getAccounts(1L);
        assertFalse(accounts.isEmpty());
        verify(accountModelRepository, times(1)).findAllByUserOrderById(user);
        verify(accountModelToAccountDTOConvert, times(1)).convert(accountModel);
    }

    @Test
    public void addAccount_notSuccess() {
        UserModel user = new UserModel().setId(1L);
        when(userModelRepository.getOne(1L)).thenReturn(user);

        AccountModel accountModel = new AccountModel().setNameAccount("name").setUser(user).setBalance(new BigDecimal("123"));
        when(accountModelRepository.save(accountModel)).thenReturn(new AccountModel());

        AccountDTO account = subj.addAccount("name", 1L, new BigDecimal("123"));

        assertNull(account);
        verify(accountModelRepository, times(1)).save(accountModel);
    }

    @Test
    public void addAccount_success() {
        UserModel userModel = new UserModel().setId(1L);
        when(userModelRepository.getOne(1L)).thenReturn(userModel);

        AccountModel accountModel = new AccountModel()
                .setNameAccount("name")
                .setUser(userModel)
                .setBalance(new BigDecimal("123"));

        when(accountModelRepository.save(accountModel)).thenReturn(accountModel);

        AccountDTO accountDTO = new AccountDTO()
                .setId(1L)
                .setNameAccount("name")
                .setUserId(1L)
                .setBalance(new BigDecimal("123"));
        when(accountModelToAccountDTOConvert.convert(accountModel)).thenReturn(accountDTO);

        AccountDTO account = subj.addAccount("name", 1L, new BigDecimal("123"));

        assertNotNull(account);
        assertEquals(accountDTO, account);

        verify(accountModelRepository, times(1)).save(accountModel);
        verify(accountModelToAccountDTOConvert, times(1)).convert(accountModel);
    }

    @Test
    public void deleteAccount_notSuccess() {
        when(accountModelRepository.getOne(1L)).thenReturn(new AccountModel());
        assertFalse(subj.deleteAccount(1L));
        verify(accountModelRepository, times(1)).getOne(1L);
    }

    @Test
    public void deleteAccount_success() {
        AccountModel accountModel = new AccountModel().setId(1L);
        when(accountModelRepository.getOne(1L)).thenReturn(accountModel);
        assertTrue(subj.deleteAccount(1L));
        verify(accountModelRepository, times(1)).getOne(1L);
    }
}