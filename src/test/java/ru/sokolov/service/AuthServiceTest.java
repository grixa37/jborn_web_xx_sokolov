package ru.sokolov.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.sokolov.api.converter.UserModelToUserDTOConverter;
import ru.sokolov.entity.UserModel;
import ru.sokolov.repository.UserModelRepository;
import ru.sokolov.security.UserRole;

import java.util.Collections;
import java.util.HashSet;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AuthServiceTest {
    @InjectMocks
    AuthService subj;
    @Mock
    UserModelRepository userModelRepository;
    @Mock
    PasswordEncoder passwordEncoder;
    @Mock
    UserModelToUserDTOConverter userModelToUserDTOConverter;

    @Test
    public void auth_userNotFound() {
        when(userModelRepository.findByEmail("email@email.ru")).thenReturn(null);

        UserDTO user = subj.auth("email@email.ru");

        assertNull(user);

        verify(userModelRepository, times(1)).findByEmail("email@email.ru");
        verifyZeroInteractions(userModelToUserDTOConverter);
    }

    @Test
    public void auth_userFound() {

        UserModel userModel = new UserModel();
        userModel.setId(1L);
        userModel.setName("name");
        userModel.setLastName("last name");
        userModel.setEmail("email@email.ru");
        userModel.setPassword("hex");
        when(userModelRepository.findByEmail("email@email.ru")).thenReturn(userModel);

        UserDTO userDTO = new UserDTO();
        userDTO.setId(1L);
        userDTO.setName("name");
        userDTO.setLastName("last name");
        userDTO.setEmail("email@email.ru");
        when(userModelToUserDTOConverter.convert(userModel)).thenReturn(userDTO);

        UserDTO user = subj.auth("email@email.ru");

        assertNotNull(user);
        assertEquals(user, userDTO);

        verify(userModelRepository, times(1)).findByEmail("email@email.ru");
        verify(userModelToUserDTOConverter, times(1)).convert(userModel);
    }

    @Test
    public void registration_notSuccess() {
        when(passwordEncoder.encode("password")).thenReturn("hex");

        UserModel userModel = new UserModel().setName("name").setLastName("last name").setEmail("email@email.ru").setPassword("hex").setRoles(new HashSet<>(Collections.singleton(UserRole.USER)));

        when(userModelRepository.save(userModel)).thenReturn(null);

        UserDTO user = subj.registration("name", "last name", "email@email.ru", "password");

        assertNull(user);

        verify(userModelRepository, times(1)).save(userModel);
    }

    @Test
    public void registration_success() {
        when(passwordEncoder.encode("password")).thenReturn("hex");

        UserModel userModel = new UserModel()
                .setName("name")
                .setLastName("last name")
                .setEmail("email@email.ru")
                .setPassword("hex")
                .setRoles(new HashSet<>(Collections.singleton(UserRole.USER)));
        when(userModelRepository.save(userModel)).thenReturn(userModel);

        UserDTO userDTO = new UserDTO();
        userDTO.setId(1L);
        userDTO.setName("name");
        userDTO.setLastName("last name");
        userDTO.setEmail("email@email.ru");
        when(userModelToUserDTOConverter.convert(userModel)).thenReturn(userDTO);

        UserDTO user = subj.registration("name", "last name", "email@email.ru", "password");

        assertNotNull(user);
        assertEquals(user, userDTO);

        verify(passwordEncoder, times(1)).encode("password");
        verify(userModelRepository, times(1)).save(userModel);
        verify(userModelToUserDTOConverter, times(1)).convert(userModel);
    }
}