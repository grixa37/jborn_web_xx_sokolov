package ru.sokolov.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import ru.sokolov.api.converter.CategoryModelToCategoryDTOConverter;
import ru.sokolov.dto.CategoryDTO;
import ru.sokolov.entity.CategoryModel;
import ru.sokolov.entity.TransactionModel;
import ru.sokolov.entity.UserModel;
import ru.sokolov.repository.CategoryModelRepository;
import ru.sokolov.repository.TransactionModelRepository;
import ru.sokolov.repository.UserModelRepository;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class CategoryServiceTest {
    @InjectMocks
    CategoryService subj;
    @Mock
    UserModelRepository userModelRepository;
    @Mock
    TransactionModelRepository transactionModelRepository;
    @Mock
    CategoryModelRepository categoryModelRepository;
    @Mock
    CategoryModelToCategoryDTOConverter converter;

    @Test
    public void addCategory_notSuccess() {
        CategoryModel categoryModel = new CategoryModel().setNameCategory("name").setUser(new UserModel().setId(1L));
        when(categoryModelRepository.saveAndFlush(categoryModel)).thenReturn(null);
        when(userModelRepository.getOne(1L)).thenReturn(new UserModel().setId(1L));
        CategoryDTO category = subj.addCategory("name", 1L);
        assertNull(category);
        verify(categoryModelRepository, times(1)).saveAndFlush(categoryModel);
    }

    @Test
    public void addCategory_success() {
        CategoryModel categoryModel = new CategoryModel();
        categoryModel.setUser(new UserModel().setId(1L));
        categoryModel.setNameCategory("name");
        when(categoryModelRepository.saveAndFlush(categoryModel)).thenReturn(categoryModel);

        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setId(1L);
        categoryDTO.setUserId(1L);
        categoryDTO.setNameCategory("name");
        when(converter.convert(categoryModel)).thenReturn(categoryDTO);
        when(userModelRepository.getOne(1L)).thenReturn(new UserModel().setId(1L));
        CategoryDTO category = subj.addCategory("name", 1L);
        assertNotNull(category);
        assertEquals(category, categoryDTO);
        verify(categoryModelRepository, times(1)).saveAndFlush(categoryModel);
        verify(converter, times(1)).convert(categoryModel);
    }

    @Test
    public void editCategory_notSuccess() {
        CategoryModel categoryModel = new CategoryModel().setNameCategory("name").setUser(new UserModel().setId(1L));
        when(categoryModelRepository.getOne(1L)).thenReturn(categoryModel);
        CategoryDTO category = subj.editCategory(1L, "name");
        assertNull(category);
        verify(categoryModelRepository, times(1)).getOne(1L);
    }

    @Test
    public void editCategory_success() {
        CategoryModel categoryModel = new CategoryModel()
                .setUser(new UserModel().setId(1L))
                .setNameCategory("name");
        when(categoryModelRepository.getOne(1L)).thenReturn(categoryModel);
        when(categoryModelRepository.save(categoryModel)).thenReturn(categoryModel);

        CategoryDTO categoryDTO = new CategoryDTO()
                .setId(1L)
                .setUserId(1L)
                .setNameCategory("name");
        when(converter.convert(categoryModel)).thenReturn(categoryDTO);

        CategoryDTO category = subj.editCategory(1L, "name");
        assertNotNull(category);
        assertEquals(category, categoryDTO);
        verify(categoryModelRepository, times(1)).getOne(1L);
        verify(converter, times(1)).convert(categoryModel);
    }

    @Test
    public void deleteCategory_notSuccess() {
        when(categoryModelRepository.getOne(1L)).thenReturn(new CategoryModel());
        assertFalse(subj.deleteCategory(1L));
        verify(categoryModelRepository, times(1)).getOne(1L);
    }

    @Test
    public void deleteCategory_success() {
        when(categoryModelRepository.getOne(1L)).thenReturn(new CategoryModel().setId(1L));
        assertTrue(subj.deleteCategory(1L));
        verify(categoryModelRepository, times(1)).getOne(1L);
    }

    @Test
    public void reports_isEmpty() {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        CategoryModel categoryModel = new CategoryModel().setId(1L);
        List<CategoryModel> categories = new ArrayList<>();
        categories.add(categoryModel);
        when(transactionModelRepository.findDistinctByCategoriesInAndDateAfterAndDateBeforeOrderById(categories, timestamp, timestamp)).thenReturn(new ArrayList<>());
        when(categoryModelRepository.getOne(1L)).thenReturn(categoryModel);
        List<String> reports = subj.reports(1L, timestamp, timestamp);
        assertTrue(reports.isEmpty());
        verify(transactionModelRepository, times(1)).findDistinctByCategoriesInAndDateAfterAndDateBeforeOrderById(categories, timestamp, timestamp);
    }

    @Test
    public void reports_notEmpty() {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        ArrayList<TransactionModel> list = new ArrayList<>();
        list.add(new TransactionModel());
        CategoryModel categoryModel = new CategoryModel().setId(1L);
        List<CategoryModel> categories = new ArrayList<>();
        categories.add(categoryModel);
        when(categoryModelRepository.getOne(1L)).thenReturn(categoryModel);
        when(transactionModelRepository.findDistinctByCategoriesInAndDateAfterAndDateBeforeOrderById(categories, timestamp, timestamp)).thenReturn(list);
        List<String> reports = subj.reports(1L, timestamp, timestamp);
        assertFalse(reports.isEmpty());
        verify(transactionModelRepository, times(1)).findDistinctByCategoriesInAndDateAfterAndDateBeforeOrderById(categories, timestamp, timestamp);
    }
}