package ru.sokolov.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.core.convert.converter.Converter;
import ru.sokolov.entity.AccountModel;
import ru.sokolov.entity.CategoryModel;
import ru.sokolov.entity.TransactionModel;
import ru.sokolov.repository.AccountModelRepository;
import ru.sokolov.repository.CategoryModelRepository;
import ru.sokolov.repository.TransactionModelRepository;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Date;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class TransactionServiceTest {
    @InjectMocks
    TransactionService subj;
    @Mock
    CategoryModelRepository categoryModelRepository;
    @Mock
    AccountModelRepository accountModelRepository;
    @Mock
    TransactionModelRepository transactionModelRepository;
    @Mock
    Converter<TransactionModel, TransactionDTO> transactionModelToTransactionDTOConvert;

    @Test
    public void addTransaction_notSuccess() {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        BigDecimal amount = new BigDecimal(10_000);
        AccountModel fromAccount = new AccountModel().setId(1L).setNameAccount("name1").setBalance(amount);
        AccountModel toAccount = new AccountModel().setId(2L).setNameAccount("name2").setBalance(amount);
        TransactionModel transactionModel = new TransactionModel()
                .setComment("comment")
                .setFromAccount(fromAccount)
                .setToAccount(toAccount)
                .setAmount(amount)
                .setDate(timestamp)
                .setCategories(null);
        when(transactionModelRepository.saveAndFlush(transactionModel)).thenReturn(null);
        when(categoryModelRepository.findAllByIdIn(Collections.singletonList(1L))).thenReturn(null);
        when(accountModelRepository.getOne(1L)).thenReturn(fromAccount);
        when(accountModelRepository.getOne(2L)).thenReturn(toAccount);
        TransactionDTO transaction = subj.addTransaction(
                "comment",
                1L,
                2L,
                amount,
                timestamp,
                Collections.singletonList(1L)
        );

        assertNull(transaction);

        verify(transactionModelRepository, times(1)).saveAndFlush(transactionModel);
    }

    @Test
    public void addTransaction_success() {
        Timestamp timestamp = new Timestamp(new Date().getTime());
        BigDecimal amount = new BigDecimal(10_000);
        AccountModel fromAccount = new AccountModel().setId(1L).setNameAccount("name1").setBalance(amount);
        AccountModel toAccount = new AccountModel().setId(2L).setNameAccount("name2").setBalance(amount);

        TransactionModel transactionModel = new TransactionModel()
                .setComment("comment")
                .setFromAccount(fromAccount)
                .setToAccount(toAccount)
                .setAmount(amount)
                .setDate(timestamp)
                .setCategories(Collections.singletonList(CategoryModel.builder().id(1L).build()));
        when(transactionModelRepository.saveAndFlush(transactionModel)).thenReturn(transactionModel);
        when(categoryModelRepository.findAllByIdIn(Collections.singletonList(1L)))
                .thenReturn(Collections.singletonList(CategoryModel.builder().id(1L).build()));
        when(accountModelRepository.getOne(1L)).thenReturn(fromAccount);
        when(accountModelRepository.getOne(2L)).thenReturn(toAccount);

        TransactionDTO transactionDTO = new TransactionDTO()
                .setId(1L)
                .setComment("comment")
                .setFromAccountId(1L)
                .setToAccountId(2L)
                .setAmount(amount)
                .setTimestamp(timestamp)
                .setCategoriesId(Collections.singletonList(1L));
        when(transactionModelToTransactionDTOConvert.convert(transactionModel)).thenReturn(transactionDTO);

        TransactionDTO transaction = subj.addTransaction(
                "comment",
                1L,
                2L,
                amount,
                timestamp,
                Collections.singletonList(1L)
        );

        assertNotNull(transaction);
        assertEquals(transaction, transactionDTO);

        verify(transactionModelRepository, times(1)).saveAndFlush(transactionModel);
        verify(transactionModelToTransactionDTOConvert, times(1)).convert(transactionModel);
    }
}