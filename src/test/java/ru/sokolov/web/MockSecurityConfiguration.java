package ru.sokolov.web;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.userdetails.UserDetailsService;
import ru.sokolov.security.CustomGrantedAuthority;
import ru.sokolov.security.CustomUserDetails;

import static java.util.Collections.singleton;
import static ru.sokolov.security.UserRole.USER;

@Configuration
public class MockSecurityConfiguration {
    @Bean
    public UserDetailsService userDetailsService() {
        return s -> new CustomUserDetails(
                1L,
                "grixa37@gmail.com",
                "rfv",
                singleton(new CustomGrantedAuthority(USER))
        );
    }
}
