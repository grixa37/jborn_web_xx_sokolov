package ru.sokolov.web.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.sokolov.config.SecurityConfiguration;
import ru.sokolov.entity.AccountModel;
import ru.sokolov.entity.UserModel;
import ru.sokolov.repository.AccountModelRepository;
import ru.sokolov.repository.UserModelRepository;
import ru.sokolov.security.UserRole;
import ru.sokolov.service.AccountService;
import ru.sokolov.web.MockSecurityConfiguration;
import ru.sokolov.web.form.AccountForm;

import java.math.BigDecimal;
import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(AccountController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
public class AccountControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserModelRepository userModelRepository;

    @MockBean
    AccountService accountService;

    @MockBean
    AccountModelRepository accountModelRepository;

    @Before
    public void setUp() {
        when(userModelRepository.getOne(1L)).thenReturn(getUser());
    }

    @Test
    public void getPersonal_Auth() throws Exception {
        mockMvc.perform(get("/accounts"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("user", getUser()))
                .andExpect(view().name("accounts"));
    }

    @Test
    public void getAddAccount_Auth() throws Exception {
        mockMvc.perform(get("/accounts/account/create"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("user", getUser()))
                .andExpect(model().attribute("addAccount", AccountForm.builder().build()))
                .andExpect(view().name("addAccount"));
    }

    @Test
    public void postAddAccount_Auth() throws Exception {
        mockMvc.perform(post("/accounts/account/create")
                        .param("id", "1")
                        .param("nameAccount", "testAccount")
                        .param("balance", "10000"))
                .andExpect(status().is(302))
                .andExpect(redirectedUrl("/accounts"));
    }

    @Test
    public void postAddAccount_Auth_FormErrors() throws Exception {
        mockMvc.perform(post("/accounts/account/create"))
                .andExpect(status().isOk())
                .andExpect(view().name("addAccount"));
    }

    @Test
    public void postDelAccount_Auth() throws Exception {
        AccountModel account = new AccountModel()
                .setId(1L)
                .setNameAccount("testAccount")
                .setBalance(new BigDecimal(10000));
        when(accountModelRepository.getOne(1L)).thenReturn(account);
        mockMvc.perform(get("/accounts/account/delete/1"))
                .andExpect(status().is(302))
                .andExpect(redirectedUrl("/accounts"));
    }

    private UserModel getUser() {
        return new UserModel()
                .setId(1L)
                .setEmail("email@gmail.com")
                .setPassword("some-password")
                .setRoles(Collections.singleton(UserRole.USER));
    }
}