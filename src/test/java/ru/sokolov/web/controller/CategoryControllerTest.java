package ru.sokolov.web.controller;

import io.florianlopes.spring.test.web.servlet.request.MockMvcRequestBuilderUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.sokolov.config.SecurityConfiguration;
import ru.sokolov.dto.CategoryDTO;
import ru.sokolov.entity.CategoryModel;
import ru.sokolov.entity.UserModel;
import ru.sokolov.repository.CategoryModelRepository;
import ru.sokolov.repository.TransactionModelRepository;
import ru.sokolov.repository.UserModelRepository;
import ru.sokolov.security.UserRole;
import ru.sokolov.service.CategoryService;
import ru.sokolov.web.MockSecurityConfiguration;
import ru.sokolov.web.form.CategoryForm;
import ru.sokolov.web.form.ReportsForm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(CategoryController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
public class CategoryControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserModelRepository userModelRepository;

    @MockBean
    CategoryService categoryService;

    @MockBean
    CategoryModelRepository categoryModelRepository;

    @MockBean
    TransactionModelRepository transactionModelRepository;

    @Before
    public void setUp() {
        when(userModelRepository.getOne(1L)).thenReturn(getUser());
    }

    @Test
    public void getCategory_Auth() throws Exception {
        List<CategoryDTO> categories = new ArrayList<>();
        CategoryDTO categoryDTO = CategoryDTO.builder()
                .nameCategory("name")
                .userId(1L)
                .id(1L)
                .build();
        categories.add(categoryDTO);
        when(categoryService.getCategories(1L)).thenReturn(categories);
        mockMvc.perform(get("/categories"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("user", getUser()))
                .andExpect(model().attribute("list", categories))
                .andExpect(view().name("category"));
    }

    @Test
    public void getReports_Auth() throws Exception {
        mockMvc.perform(get("/categories/report/creat"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("user", getUser()))
                .andExpect(model().attribute("reports", new ReportsForm()))
                .andExpect(view().name("getDataReports"));
    }

    @Test
    public void postReports_Auth() throws Exception {
        ArrayList<CategoryModel> categoryModels = new ArrayList<>();
        categoryModels.add(CategoryModel.builder().id(1L).build());
        ReportsForm reportsForm = new ReportsForm();
        reportsForm.setId(1L).setDateAfter("2020-10-10").setDateBefore("2020-10-10").setCategories(categoryModels);
        mockMvc.perform(post("/categories/report/creat")
                        .with(MockMvcRequestBuilderUtils.form(reportsForm)))
                .andExpect(status().isOk())
                .andExpect(view().name("reports"));
    }

    @Test
    public void postReports_Auth_FormErrors() throws Exception {
        mockMvc.perform(post("/categories/report/creat"))
                .andExpect(status().isOk())
                .andExpect(view().name("getDataReports"));
    }

    @Test
    public void getAddCategory_Auth() throws Exception {
        mockMvc.perform(get("/categories/category/creat"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("user", getUser()))
                .andExpect(model().attribute("form", new CategoryForm()))
                .andExpect(view().name("addCategory"));
    }

    @Test
    public void postAddCategory_Auth() throws Exception {
        mockMvc.perform(post("/categories/category/creat")
                        .param("id", "1")
                        .param("nameCategory", "TestCategory"))
                .andExpect(status().is(302))
                .andExpect(redirectedUrl("/categories"));
    }

    @Test
    public void postAddCategory_Auth_FormErrors() throws Exception {
        mockMvc.perform(post("/categories/category/creat"))
                .andExpect(status().isOk())
                .andExpect(view().name("addCategory"));
    }

    @Test
    public void getEditCategory_Auth() throws Exception {
        when(categoryModelRepository.getOne(1L))
                .thenReturn(CategoryModel.builder().id(1L).build());
        mockMvc.perform(get("/categories/category/update/1"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("user", getUser()))
                .andExpect(model().attribute("category", new CategoryForm().setId(1L)))
                .andExpect(view().name("editCategory"));
    }

    @Test
    public void postEditCategory_Auth() throws Exception {
        when(categoryModelRepository.getOne(1L)).thenReturn(
                new CategoryModel()
                        .setId(1L)
                        .setNameCategory("TestCategory")
                        .setUser(getUser())
        );
        mockMvc.perform(post("/categories/category/update/1")
                        .param("id", "1")
                        .param("nameCategory", "TestCategory"))
                .andExpect(status().is(302))
                .andExpect(redirectedUrl("/categories"));
    }

    @Test
    public void postEditCategory_Auth_FormErrors() throws Exception {
        mockMvc.perform(post("/categories/category/update/1"))
                .andExpect(status().isOk())
                .andExpect(view().name("editCategory"));
    }


    @Test
    public void postDelCategory_Auth() throws Exception {
        when(categoryModelRepository.getOne(1L)).thenReturn(
                new CategoryModel()
                        .setId(1L)
                        .setNameCategory("TestCategory")
                        .setUser(getUser())
        );
        mockMvc.perform(get("/categories/category/delete/1")
                        .param("id", "1")
                        .param("nameCategory", "TestCategory"))
                .andExpect(status().is(302))
                .andExpect(redirectedUrl("/categories"));
    }

    private UserModel getUser() {
        return new UserModel()
                .setId(1L)
                .setEmail("email.gmail.com")
                .setPassword("some-password")
                .setRoles(Collections.singleton(UserRole.USER));
    }
}