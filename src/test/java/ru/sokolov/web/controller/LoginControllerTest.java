package ru.sokolov.web.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.sokolov.config.SecurityConfiguration;
import ru.sokolov.entity.UserModel;
import ru.sokolov.repository.UserModelRepository;
import ru.sokolov.security.UserRole;
import ru.sokolov.web.MockSecurityConfiguration;

import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(LoginController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
public class LoginControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserModelRepository userModelRepository;

    @WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
    @Test
    public void index_Auth() throws Exception {
        when(userModelRepository.getOne(1L)).thenReturn(
                new UserModel()
                        .setId(1L)
                        .setEmail("grixa37@gmail.com")
                        .setPassword("some-password")
                        .setRoles(Collections.singleton(UserRole.USER))
        );
        mockMvc.perform(get("/"))
                .andExpect(status().is(302))
                .andExpect(redirectedUrl("/accounts"));
    }

    @Test
    public void getLogin() throws Exception {
        mockMvc.perform(get("/login"))
                .andExpect(status().isOk())
                .andExpect(view().name("login"));
    }
}