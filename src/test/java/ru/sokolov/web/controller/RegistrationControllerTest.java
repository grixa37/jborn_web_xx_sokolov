package ru.sokolov.web.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.sokolov.config.SecurityConfiguration;
import ru.sokolov.repository.UserModelRepository;
import ru.sokolov.web.MockSecurityConfiguration;
import ru.sokolov.web.form.UserForm;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(RegistrationController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
public class RegistrationControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    UserModelRepository userModelRepository;

    @Test
    public void getRegistration() throws Exception {
        mockMvc.perform(get("/registration"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("form", new UserForm()))
                .andExpect(view().name("registration"));
    }

    @Test
    public void postRegistration_Ok() throws Exception {
        mockMvc.perform(post("/registration")
                        .param("email", "email@gmail.com")
                        .param("password", "password"))
                .andExpect(status().is(302))
                .andExpect(redirectedUrl("/accounts"));
    }

    @Test
    public void postRegistration_NotOk() throws Exception {
        mockMvc.perform(post("/registration"))
                .andExpect(status().isOk())
                .andExpect(view().name("/registration"));
    }
}