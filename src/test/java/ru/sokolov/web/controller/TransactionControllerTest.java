package ru.sokolov.web.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.florianlopes.spring.test.web.servlet.request.MockMvcRequestBuilderUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.sokolov.config.SecurityConfiguration;
import ru.sokolov.entity.AccountModel;
import ru.sokolov.entity.UserModel;
import ru.sokolov.repository.AccountModelRepository;
import ru.sokolov.repository.CategoryModelRepository;
import ru.sokolov.repository.TransactionModelRepository;
import ru.sokolov.repository.UserModelRepository;
import ru.sokolov.security.UserRole;
import ru.sokolov.service.TransactionService;
import ru.sokolov.web.MockSecurityConfiguration;
import ru.sokolov.web.form.TransactionForm;

import java.math.BigDecimal;
import java.util.Collections;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(TransactionController.class)
@Import({SecurityConfiguration.class, MockSecurityConfiguration.class})
@WithUserDetails(userDetailsServiceBeanName = "userDetailsService")
public class TransactionControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    TransactionService transactionService;

    @MockBean
    UserModelRepository userModelRepository;

    @MockBean
    AccountModelRepository accountModelRepository;

    @MockBean
    TransactionModelRepository transactionModelRepository;

    @MockBean
    CategoryModelRepository categoryModelRepository;

    @Before
    public void setUp() {
        when(userModelRepository.getOne(1L)).thenReturn(getUser());
    }

    @Test
    public void getAddTransaction_Auth() throws Exception {
        mockMvc.perform(get("/transactions/transaction/creat"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("user", getUser()))
                .andExpect(model().attribute("addTransaction", TransactionForm.builder().build()))
                .andExpect(view().name("transaction"));
    }

    @Test
    public void postAddTransaction_Auth() throws Exception {
        when(accountModelRepository.getOne(1L)).thenReturn(new AccountModel().setBalance(new BigDecimal(20000)));
        when(accountModelRepository.getOne(2L)).thenReturn(new AccountModel().setBalance(new BigDecimal(20000)));

        TransactionForm transactionForm = TransactionForm.builder()
                .comment("comment")
                .fromAccountId(1L)
                .toAccountId(2L)
                .amount(new BigDecimal(20000))
                .categoriesId(Collections.singletonList(1L))
                .build();

        mockMvc.perform(post("/transactions/transaction/creat")
                        .with(MockMvcRequestBuilderUtils.form(transactionForm)))
                .andExpect(status().is(302))
                .andExpect(redirectedUrl("/transactions/transaction/creat"));
    }

    @Test
    public void postAddTransaction_Auth_FormErrors() throws Exception {
        mockMvc.perform(post("/transactions/transaction/creat"))
                .andExpect(status().isOk())
                .andExpect(view().name("transaction"));
    }

    private UserModel getUser() {
        return new UserModel()
                .setId(1L)
                .setEmail("email.gmail.com")
                .setPassword("some-password")
                .setRoles(Collections.singleton(UserRole.USER));
    }
}